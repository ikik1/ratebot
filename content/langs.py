import importlib
import storage as db


class Message:

    def __init__(self, uid, lang='en'):

        uid = str(uid)

        bl = db.get_botlang(uid)

        if (not bl or (lang and lang != bl.Lang)):
            bl = db.save_botlang(uid, lang)

        self._bot_strings = importlib.import_module(
                                'content.bot.%s.strings' % bl.Lang)
        print(self._bot_strings)
        self._wallet_strings = \
            importlib.import_module('content.wallet.%s.strings' % bl.Lang)

    def get_welcome(self):
        attr = getattr(self._bot_strings, '_Welcome')
        return attr

    def get_menu(self):
        attr = getattr(self._bot_strings, '_Menu')
        return attr

    def format_amount(self, o):
        amount = o.CreditAmount
        if (o.CreditAmount == 0):
            amount = o.DebitAmount

        return "{0} {1}".format(amount, o.Coin.upper())

    def format_id(self, id):
        return "..." + id[-3:]

    def get_op_direction(self, op, uid, in_out):
        if (op.Type == 2 and op.UserId == uid):
            return in_out[1]
        elif (op.CreditAmount > 0):
            return in_out[0]
        else:
            return in_out[1]

    def get_notification(self, o, uid):
        attr = getattr(self._wallet_strings, '_Ops_Detail')
        tps = getattr(self._wallet_strings, '_Types')
        in_out = getattr(self._wallet_strings, '_IN_OUT')
        mes = attr.format(
                    tps[o.Type],
                    o.CreatedAt,
                    self.format_amount(o),
                    self.format_id(str(o.Id)),
                    self.get_op_direction(o, uid, in_out))

        return mes

    def get_wallet_summary(self, wallets):
        mes = ''
        attr = getattr(self._wallet_strings, '_Wallet_Summary')
        for w in wallets:
            mes += attr.format(w.Address, '{0:f}'.format(w.Balance),
                                          '{0:f}'.format(w.Hold),
                                          w.Coin.upper())
        return mes

    def get_deposit_currency(self):
        return getattr(self._wallet_strings, '_TopUpCurrency')

    def get_buy_coin(self):
        return getattr(self._wallet_strings, '_BuyCoin')

    def get_sell_coin(self):
        return getattr(self._wallet_strings, '_SellCoin')

    def get_topup_fiat_amount(self, coin):
        attr = getattr(self._wallet_strings, '_TopUpFiatAmount')
        return attr.format(coin.upper())

    def get_wallet_address(self, coin):
        attr = getattr(self._wallet_strings, '_TopUpAddress')
        return attr.format(coin.upper())

    def get_wallet_address_info(self, address):
        attr = getattr(self._wallet_strings, '_TopUpAddressInfo')
        return attr.format(address)

    def get_withdraw_coin_amount(self, coin):
        attr = getattr(self._wallet_strings, '_WithdrawCoinAmount')
        return attr.format(coin.upper())

    def get_buy_coin_amount(self, coin):
        attr = getattr(self._wallet_strings, '_BuyCoinAmount')
        return attr.format(coin.upper())

    def get_sell_coin_amount(self, coin):
        attr = getattr(self._wallet_strings, '_SellCoinAmount')
        return attr.format(coin.upper())

    def get_second_coin(self, cmd):
        parts = cmd.split('_')
        attr = getattr(self._wallet_strings, '_SecondCoin')
        sell_buy = getattr(self._wallet_strings, '_SELL_BUY')
        return attr.format(sell_buy[parts[0]], parts[1].upper(), parts[2])

    def get_withdraw_address(self, cmd):
        parts = cmd.split('_')
        attr = getattr(self._wallet_strings, '_WithdrawAddress')
        return attr.format(parts[1].upper())

    def get_confirmation_withdraw(self, amount, coin, address):
        attr = getattr(self._wallet_strings, '_WithdrawConfirm')
        return attr.format(amount, coin.upper(), address)

    def get_alipay_progress(self):
        return getattr(self._wallet_strings, '_AliPayProgress')

    def get_alipay_topup(self):
        return getattr(self._wallet_strings, '_AliPayTopUp')

    def get_withdrawal_fiat_info(self):
        return getattr(self._wallet_strings, '_FiatWithdrawalInfo')

    def get_confirmation_buy_trade(self, amount, first_coin, rate, second_coin,
                                   total_to_spend, cmd, balance):
        attr = getattr(self._wallet_strings, '_ConfirmBuyTrade')
        return attr.format(amount, first_coin.upper(),
                           rate, second_coin.upper(),
                           total_to_spend, cmd, balance)

    def get_confirmation_sell_trade(self, amount, first_coin, rate,
                                    second_coin, total_to_spend, cmd, balance):
        attr = getattr(self._wallet_strings, '_ConfirmSellTrade')
        return attr.format(amount, first_coin.upper(),
                           rate, second_coin.upper(),
                           total_to_spend, cmd, balance)

    def get_confirm_withdrawal(self, tx, amount, coin, address):
        attr = getattr(self._wallet_strings, '_WithdrawalSuccess')
        return attr.format(tx, amount, coin, address)

    def get_cancel_trade(self):
        return getattr(self._wallet_strings, '_CancelTrade')

    def get_main(self):
        return getattr(self._bot_strings, '_Main')

    def get_trade_optype(self, t):
        types = getattr(self._wallet_strings, '_TradeTypes')

        if (t['OperationType'] in types):
            return types[t['OperationType']]
        else:
            return types[-1]

    def get_trade_summary(self, res, symbol):

        attr = getattr(self._wallet_strings, '_ExchangeLog')
        _TradeLog = getattr(self._wallet_strings, '_TradeLog')
        _SProc = getattr(self._wallet_strings, '_SProc')
        _SProcClosed = getattr(self._wallet_strings, '_SProcClosed')

        t_log = ''
        trs = res["TradeOperations"]
        closed = False
        for t in trs:
            closed = (t["OperationType"] == 3)
            t_log += _TradeLog.format(self.get_trade_optype(t), t["Message"])

        s_processing = _SProc

        if (closed):
            s_processing = _SProcClosed

        return attr.format(t_log, symbol, s_processing)

    def get_ops_summary(self, ops, uid):
        attr = getattr(self._wallet_strings, '_Ops_Summary')
        tps = getattr(self._wallet_strings, '_Types')
        in_out = getattr(self._wallet_strings, '_IN_OUT')
        mes = ''
        for o in ops:
            mes += attr.format(tps[o.Type],
                               o.CreatedAt,
                               self.format_amount(o),
                               self.format_id(str(o.Id)),
                               self.get_op_direction(o, uid, in_out))

        return mes

    def get_choose_market_text(self):
            return getattr(self._bot_strings, '_Market')

    def get_orders_list_text(self, orders, pair):
        mes = ''

        order_item = getattr(self._bot_strings, '_Market_Order')
        order_t = getattr(self._bot_strings, '_Order_Types')
        om = ''
        for o in orders:
            om += order_item.format(order_t[o["orderType"]], o["volume"], o["price"])

        mes = getattr(self._bot_strings, '_Market_Orders')
        return mes.format(pair, om)
