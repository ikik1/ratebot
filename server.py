import sys
import logging
import jsonschema as js
from flask import Flask, request, Blueprint, url_for, Response, render_template
from flask_restplus import Api, Resource
from flask import jsonify
from functools import wraps
from wallet.operations import WalletOps
import wallet.storage as db
import wallet.config as cfg
from decimal import Decimal
from gateways.onepay.connector import OnePayConnector
from gateways.paystack.connector import NgnConnector
import wallet.operations as ops
import json
from dotenv import load_dotenv
import os
import messages as global_mes
from bson import ObjectId
from datetime import datetime
from wallet.withdrawal import WithdrawalManager

load_dotenv()

logging.basicConfig(
                    format=global_mes.LogFormat,
                    level=logging.INFO)

logger = logging.getLogger(__name__)


class Custom_API(Api):
    @property
    def specs_url(self):
        return url_for(self.endpoint('specs'), _external=False)


app = Flask(__name__)
blueprint = Blueprint('api', __name__, url_prefix='/api')
api = Custom_API(blueprint, validate=True, doc='/doc/')
app.register_blueprint(blueprint)

app.logger.addHandler(logging.StreamHandler(sys.stdout))
app.logger.setLevel(logging.DEBUG)

# JSON schemas
topupfiatAliPaySchema = api.schema_model('TopUpFiat', {
    "type": "object",
    "properties": {
        "request-service": {
            "type": "string",
            "pattern": "^order-book$"
        },
        "operation-id": {
            "type": "string",
            "minLength": 3,
            "maxLength": 20
        },
        "user-id": {
            "type": "string",
            "minLength": 3,
            "maxLength": 55
        },
        "amount": {
            "type": "number",
            "minimum": 0
        },
        "currency": {
            "type": "string",
            "minLength": 3,
            "maxLength": 20
        }
    },
    "required": ["request-service", "operation-id", "user-id", "amount",
                 "currency"]
})

holdSchema = api.schema_model('Hold', {
    "type": "object",
    "properties": {
        "request-service": {
            "type": "string",
            "pattern": "^order-book$"
        },
        "operation-id": {
            "type": "string",
            "minLength": 3,
            "maxLength": 20
        },
        "user-id": {
            "type": "string",
            "minLength": 3,
            "maxLength": 55
        },
        "amount": {
            "type": "number",
            "minimum": 0
        },
        "currency": {
            "type": "string",
            "minLength": 3,
            "maxLength": 20
        }
    },
    "required": ["request-service", "operation-id", "user-id", "amount",
                 "currency"]
})


exchnageSchema = api.schema_model('Exchange', {
    "type": "object",
    "properties": {
        "request-service": {
            "type": "string",
            "pattern": "^order-book$"
        },
        "operation-id": {
            "type": "string",
            "minLength": 3,
            "maxLength": 55
        },
        "hold-id-from": {
            "type": "string",
            "minLength": 3,
            "maxLength": 55
        },
        "amount-from": {
            "type": "number",
            "minimum": 0
        },
        "hold-id-to": {
            "type": "string",
            "minLength": 3,
            "maxLength": 55
        },
        "amount-to": {
            "type": "number",
            "minimum": 0
        }
    },
    "required": ["request-service", "operation-id", "hold-id-from",
                 "amount-from", "hold-id-to", "amount-to"]
})


releaseSchema = api.schema_model('Release', {
    "type": "object",
    "properties": {
        "request-service": {
            "type": "string",
            "pattern": "^order-book$"
        },
        "operation-id": {
            "type": "string",
            "minLength": 3,
            "maxLength": 55
        },
        "hold-id": {
                    "type": "string",
                    "minLength": 3,
                    "maxLength": 55
        },
        "amount": {
                    "type": "number",
                    "minimum": 0
                  }
    },
    "required": ["request-service", "operation-id", "hold-id", "amount"]
})


writeoffSchema = api.schema_model('WriteOff', {
    "type": "object",
    "properties": {
        "request-service": {
            "type": "string",
            "pattern": "^order-book$"
        },
        "operation-id": {
            "type": "string",
            "minLength": 3,
            "maxLength": 55
        },
        "hold-id": {
                    "type": "string",
                    "minLength": 3,
                    "maxLength": 55
        },
        "amount": {
                    "type": "number",
                    "minimum": 0
                  }
    }
})


walletCreatorSchema = api.schema_model('WalletCreator', {
    "type": "object",
    "properties": {
        "request-service": {
            "type": "string",
            "pattern": "^order-book$"
        },
        "user-id": {
            "type": "string",
            "minLength": 3,
            "maxLength": 55
        }
    },
    "required": ["user-id"]
})


withdrawSchema = api.schema_model('Withdrawal', {
    "type": "object",
    "properties": {
        "request-service": {
            "type": "string",
            "pattern": "^order-book$"
        },
        "user-id": {
            "type": "string",
            "minLength": 3,
            "maxLength": 55
        },
        "currency": {
            "type": "string",
            "minLength": 3,
            "maxLength": 20
        },
        "amount": {
            "type": "string",
            "minLength": 1
        },
        "address": {
            "type": "string",
            "minLength": 26,
            "maxLength": 42
        }
    },
    "required": ["user-id", "currency", "amount", "address"]
})


withdrawFiatRequestSchema = api.schema_model('Withdrawal', {
    "type": "object",
    "properties": {
        "request-service": {
            "type": "string",
            "pattern": "^order-book$"
        },
        "user-id": {
            "type": "string",
            "minLength": 3,
            "maxLength": 55
        },
        "currency": {
            "type": "string",
            "minLength": 3,
            "maxLength": 20
        },
        "amount": {
            "type": "string",
            "minLength": 1
        }
    },
    "required": ["user-id", "currency", "amount"]
})


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime):
            return str(o)
        if isinstance(o, Decimal):
            return str(o)
        return json.JSONEncoder.default(self, o)


def validate_schema(schema):
    validator = js.Draft4Validator(schema)

    def wrapper(fn):
        @wraps(fn)
        def wrapped(*args, **kwargs):
            input = request.get_json(force=True)
            errors = [error.message for error in validator.iter_errors(input)]
            if errors:
                response = jsonify(dict(result=False,
                                        message="invalid input",
                                        errors=errors))
                app.logger.info(response)
                response.status_code = 400
                return response
            else:
                return fn(*args, **kwargs)
        return wrapped
    return wrapper


@api.route('/ping')
class Ping(Resource):
    def get(self):
        return {'result': 'true'}


@api.route('/topup/fiat/ngn')
class TopUpFiatNgn(Resource):

    # @api.expect(topupfiatAliPaySchema)
    def get(self):

        try:
            assert "uid" in request.args, "No uid parameter"
            assert "coin" in request.args, "No uid parameter"
            assert "amount" in request.args, "No uid parameter"

            uid = request.args["uid"]
            coin = request.args["coin"]

            ac = NgnConnector(uid, coin, logger=app.logger)

            payload = ac.get_payload(request.args['amount'])
            payload['uid'] = uid
            payload['ret_url'] = request.args['ret_url']
            res = Response(render_template('paystack/topup.html', payload=payload))

            res.headers.set('Content-Type', 'text/html')
            return res
        except AssertionError as e:
                    res = jsonify({'result': 'false', 'message': str(e)})
                    res.status_code = 400


@api.route('/topup/fiat/onepay')
class TopUpFiat(Resource):

    @api.expect(topupfiatAliPaySchema)
    def post(self):
        req = request.get_json()
        al = OnePayConnector(req["user-id"], req["currency"],
                             logger=app.logger)
        res = Response(al.get_payment_qr(req["amount"]))
        res.headers.set('Content-Type', 'image/jpeg')
        return res

    # @api.expect(topupfiatAliPaySchema)
    def get(self):

        try:
            assert "uid" in request.args, "No uid parameter"
            assert "coin" in request.args, "No uid parameter"
            assert "amount" in request.args, "No uid parameter"

            uid = request.args["uid"]
            coin = request.args["coin"]

            ac = OnePayConnector(uid, coin, logger=app.logger)

            payload = ac.get_payload(request.args['amount'])

            res = Response(render_template('onepay/topup.html', payload=payload))

            res.headers.set('Content-Type', 'text/html')

        except AssertionError as e:
                    res = jsonify({'result': 'false', 'message': str(e)})
                    res.status_code = 400
        return res


@api.route('/topup/callback/fiat/alipay')
class TopUpFiatAlipay(Resource):
    def _topup(self, req):

        prq = db.get_fiatpaymentrequest_by_ref(req.form['orderid'])

        assert prq is not None, "Request not found"
        assert prq.Cfms == 0, "Request already confirmed"

        ac = OnePayConnector(prq.UserId, prq.Coin, logger=app.logger)
        op = ac.confirm(prq, req.form['transaction_id'])

        return op is not None

    # @api.expect(topupfiatSchema)
    def post(self):

        # check signature from the server

        app.logger.info("Request: %s" % request.form['sign'])
        if (self._topup(request)):
            return Response('OK')
        else:
            return Response('False')

    def get(self):
        if (self._topup(request)):
            return Response('OK')
        else:
            return Response('False')


@api.route('/topup/callback/fiat/paystack')
class TopUpFiatPayStack(Resource):

    def _success(self, req):

        prq = db.get_fiatpaymentrequest_by_ref(req['data']['reference'])

        assert prq is not None, "Request not found"
        assert prq.Cfms == 0, "Request already confirmed"

        ac = NgnConnector(prq.UserId, prq.Coin, logger=app.logger)
        op = ac.confirm(prq, req['data']['reference'])

        return op is not None

    def post(self):

        req = request.get_json(force=True)

        app.logger.info("JSON: %s" % req['event'])

        assert "event" in req, "Invalid callback."

        event = {
            "charge.success": self._success
        }

        proc = event[req["event"]]
        proc(req)

        return Response('OK')


@api.route('/topup')
class TopUp(Resource):

    def __notify(self, op, target_wallet=None):
        db.add_opnotification(op.UserId, op.Id)

    def __is_target_address(self, a, ins):
        for i in ins:
            if a in i["addresses"]:
                return False
        return True

    def post(self):

        req = request.get_json(force=True)

        # Parsing callback routine
        # Getting destination address
        outs = req['outputs']
        ins = req['inputs']
        ref = req['hash']

        assert len(outs) > 0, "Outputs must have more than zero records"
        assert len(ins) > 0, "Inputs must have more than zero records"
        assert len(ref) > 0, "Transaction hash should be more than zero length"

        op_id = ''
        for o in outs:
            val = o["value"]
            addrs = o["addresses"]
            for a in addrs:
                if (self.__is_target_address(a, ins)):
                    try:
                        wo = WalletOps(addr=a, coin=cfg.BC_COIN)
                        op_id = wo.roll(db.OpTypes.TopUp, Decimal(str(val)),
                                        reference=ref, callback=self.__notify)
                    except AssertionError as e:
                        app.logger.info(e)

        return jsonify({'result': 'true', 'op-id': str(op_id.Id)})


@api.route('/hold')
class Hold(Resource):
    @api.expect(holdSchema)
    def post(self):
        hold_req = request.get_json()
        try:
            wo = WalletOps(coin=hold_req['currency'].lower(),
                           uid=hold_req['user-id'])

            op = wo.roll(db.OpTypes.Hold, Decimal(str(hold_req['amount'])),
                         reference=hold_req['operation-id'],
                         to_norm_dec=False)

            res = jsonify({'result': 'true', 'message': "success",
                           'hold-id': str(op.Id)})
        except AssertionError as e:
            res = jsonify({'result': 'false', 'message': str(e)})
            res.status_code = 400
        return res


@api.route('/exchange')
class Exchange(Resource):

    def __notify(self, op, target_wallet=None):
        db.add_opnotification(op.UserId, op.Id)
        if (target_wallet is not None):
            db.add_opnotification(target_wallet.UserId, op.Id)

    @api.expect(exchnageSchema)
    def post(self):
        hold_req = request.get_json()
        try:

            db.check_hold_balance(hold_req['hold-id-from'],
                                  Decimal(str(hold_req['amount-from'])),
                                  update=False)

            db.check_hold_balance(hold_req['hold-id-to'],
                                  Decimal(str(hold_req['amount-to'])),
                                  update=False)

            wo_from = WalletOps(op_id=hold_req['hold-id-from'])
            wo_to = WalletOps(op_id=hold_req['hold-id-to'])

            # getting wallet with the same currency as wo_from
            wo_target = WalletOps(uid=wo_to.wallet.UserId,
                                  coin=wo_from.wallet.Coin)

            op_from = wo_from.roll(db.OpTypes.Transfer,
                                   Decimal(str(hold_req['amount-from'])),
                                   target_wallet=wo_target.wallet,
                                   reference=hold_req['hold-id-from'],
                                   to_norm_dec=False,
                                   callback=self.__notify)

            # getting wallet with the same currency as wo_to
            wo_target = WalletOps(uid=wo_from.wallet.UserId,
                                  coin=wo_to.wallet.Coin)

            op_to = wo_to.roll(db.OpTypes.Transfer,
                               Decimal(str(hold_req['amount-to'])),
                               target_wallet=wo_target.wallet,
                               reference=hold_req['hold-id-to'],
                               to_norm_dec=False,
                               callback=self.__notify)

            res = jsonify({'result': 'true', 'message': "success",
                           'op-id-from': str(op_from.Id),
                           'op-id-to': str(op_to.Id)})

        except AssertionError as e:
            res = jsonify({'result': 'false', 'message': str(e)})
            res.status_code = 400

        return res


@api.route('/release')
class Release(Resource):

    def __notify(self, op, target_wallet=None):
        db.add_opnotification(op.UserId, op.Id)

    @api.expect(releaseSchema)
    def post(self):
        try:
            hold_req = request.get_json()
            wo = WalletOps(op_id=hold_req['hold-id'])
            op = wo.roll(db.OpTypes.Release, hold_req['amount'],
                         reference=hold_req['hold-id'],
                         to_norm_dec=False, callback=self.__notify)

            res = jsonify({'result': 'true', 'message': "success",
                           'op-id': str(op.Id)})

        except AssertionError as e:
            res = jsonify({'result': 'false', 'message': str(e)})
            res.status_code = 400

        return res


@api.route('/writeoff')
class WriteOff(Resource):

    def __notify(self, op, target_wallet=None):
        db.add_opnotification(op.UserId, op.Id)

    @api.expect(writeoffSchema)
    def post(self):
        try:
            hold_req = request.get_json()
            wo = WalletOps(op_id=hold_req['hold-id'])
            op = wo.roll(db.OpTypes.WriteOff, hold_req['amount'],
                         reference=hold_req['hold-id'],
                         to_norm_dec=False, callback=self.__notify)

            res = jsonify({'result': 'true', 'message': "success",
                           'op-id': str(op.Id)})
        except AssertionError as e:
            res = jsonify({'result': 'false', 'message': str(e)})
            res.status_code = 400

        return res


@api.route('/withdraw/fiat/request_withdraw')
class WithdrawFiatRequest(Resource):

    @api.expect(withdrawFiatRequestSchema)
    def post(self):

        req = request.get_json()

        try:
            uid = req['user-id']
            coin = req['currency']
            amount = req['amount']

            ac = OnePayConnector(uid, coin, logger=app.logger)
            ac.get_payout_req(amount)

            res = jsonify({'result': 'true', 'id': ac.get_payout_req(amount)})
            return res
        except AssertionError as e:
            res = jsonify({'result': 'false', 'message': str(e)})
            res.status_code = 400
            return res


@api.route('/withdraw/fiat/paystack/form')
class WithdrawPayStackFiatFormRenderer(Resource):

    def __notify(self, op, target_wallet=None):
        db.add_opnotification(op.UserId, op.Id)

    def get(self):

        try:
            assert "id" in request.args, "No id parameter"
            assert "uid" in request.args, "No uid parameter"

            uid = request.args["uid"]
            pm = db.get_fiatpaymentrequest_by_ref(request.args["id"])

            assert pm is not None, "Payment request not found"
            assert pm.UserId == uid, "Payment request is not yours"
            assert pm.Cfms == 0, "Payment already confirmed"

            name = request.args["id"]
            app.logger.info('Name %s' % name)
            res = Response(render_template('paystack/index.html',
                                           order_id=str(pm.Id),
                                           amount=str(pm.Amount)))
            res.headers.set('Content-Type', 'text/html')
            return res

        except AssertionError as e:
            res = jsonify({'result': 'false', 'message': str(e)})
            res.status_code = 400
            return res

    def post(self):

        prq = db.get_fiatpaymentrequest_by_ref(request.form['orderid'])

        assert prq is not None, "Request not found"
        assert prq.Cfms == 0, "Request already confirmed"

        ac = NgnConnector(prq.UserId, prq.Coin, logger=app.logger)

        wo = ops.WalletOps(coin=prq.Coin, uid=prq.UserId)
        hold_op = wo.roll(db.OpTypes.Hold, prq.Amount,
                          reference=prq.Id,
                          to_norm_dec=False)
        resp = None
        try:
            resp = ac.do_payout_request(str(prq.Amount),
                                        request.form["accountname"],
                                        request.form["cardnumber"],
                                        request.form["bvn"],
                                        request.form["address"],
                                        request.form["email"],
                                        str(hold_op.Id))

            if (resp):
                w_op = wo.roll(db.OpTypes.Withdraw, prq.Amount,
                               reference=resp["transaction_id"],
                               callback=self.__notify,
                               to_norm_dec=False)

                if (w_op):
                    db.confirm_fiatpaymentrequest(prq.Id)
        except Exception as e:
            resp = None
            app.logger.exception(e)

        if (not resp):
            resp = jsonify({'status': False,
                            'message': "Cant't make withdrawal"})
            app.logger.info(hold_op.__dict__)
            return resp

        res_html = Response(render_template('paystack/ok.html',
                                            status=resp['status'],
                                            message=resp['msg'],
                                            tx=resp['transaction_id']))
        res_html.headers.set('Content-Type', 'text/html')
        return res_html


@api.route('/withdraw/fiat/alipay/form')
class WithdrawFiatFormRenderer(Resource):

    def __notify(self, op, target_wallet=None):
        db.add_opnotification(op.UserId, op.Id)

    def get(self):

        try:
            assert "id" in request.args, "No id parameter"
            assert "uid" in request.args, "No uid parameter"

            uid = request.args["uid"]

            pm = db.get_fiatpaymentrequest_by_ref(request.args["id"])

            assert pm is not None, "Payment request not found"
            assert pm.UserId == uid, "Payment request is not yours"
            assert pm.Cfms == 0, "Payment already confirmed"

            name = request.args["id"]
            app.logger.info('Name %s' % name)
            res = Response(render_template('alipay/index.html',
                                           order_id=str(pm.Id),
                                           amount=str(pm.Amount)))
            res.headers.set('Content-Type', 'text/html')
            return res
        except AssertionError as e:
            res = jsonify({'result': 'false', 'message': str(e)})
            res.status_code = 400
            return res

    def post(self):

        prq = db.get_fiatpaymentrequest_by_ref(request.form['orderid'])

        assert prq is not None, "Request not found"
        assert prq.Cfms == 0, "Request already confirmed"

        ac = OnePayConnector(prq.UserId, prq.Coin, logger=app.logger)

        wo = ops.WalletOps(coin=prq.Coin, uid=prq.UserId)
        hold_op = wo.roll(db.OpTypes.Hold, prq.Amount,
                          reference=prq.Id,
                          to_norm_dec=False)
        resp = None
        try:
            resp = ac.do_payout_request(str(prq.Amount),
                                        request.form["bankname"],
                                        request.form["subbranch"],
                                        request.form["accountname"],
                                        request.form["cardnumber"],
                                        request.form["province"],
                                        request.form["city"],
                                        request.form["bank_number"],
                                        str(hold_op.Id))

            r_json = resp.json()

            if (r_json["status"] == "success"):
                w_op = wo.roll(db.OpTypes.Withdraw, prq.Amount,
                               reference=r_json["transaction_id"],
                               callback=self.__notify,
                               to_norm_dec=False)

                if (w_op):
                    db.confirm_fiatpaymentrequest(prq.Id)
        except Exception as e:
            resp = None
            app.logger.exception(e)

        if (not resp):
            resp = jsonify({'status': False,
                            'message': "Cant't make withdrawal"})
            app.logger.info(hold_op.__dict__)
            return resp

        resp_json = json.loads(resp.content)
        res_html = Response(render_template('alipay/ok.html',
                                            status=resp_json['status'],
                                            message=resp_json['msg'],
                                            tx=resp_json
                                            .get('transaction_id')))
        res_html.headers.set('Content-Type', 'text/html')
        return res_html


@api.route('/withdraw')
class Withdrawal(Resource):

    @api.expect(withdrawSchema)
    def post(self):

        req = request.get_json()

        try:

            uid = req['user-id']
            coin = req['currency'].lower()
            amount = Decimal(req['amount'])
            address = req['address']

            w_mng = WithdrawalManager(coin, uid)
            tx = None
            op = w_mng.request_withdraw(amount, address)
            tx = w_mng.confirm_withdraw(op, Decimal(amount))
            res = jsonify({'result': 'true', 'message': "success",
                           'tx': json.dumps(tx)})
        except AssertionError as e:
            res = jsonify({'result': 'false', 'message': str(e)})
            res.status_code = 400

        return res


@api.route('/wallets')
class WalletCreator(Resource):

    @api.expect(walletCreatorSchema)
    def post(self):

        req = request.get_json()

        try:

            uid = req['user-id']
            wallets = WalletOps.create_wallets(uid)

            # temprorary solution
            cn = OnePayConnector(str(uid), 'cny', logger=logger)
            ngn = NgnConnector(str(uid), 'ngn', logger=logger)

            wallets += [cn.wallet, ngn.wallet]

            wjson = []
            for w in wallets:
                w.PrivateKey = w.PrivateKey[-6:]
                wjson.append(JSONEncoder().encode(w.__dict__))

            res = jsonify({'result': 'true', 'message': "success",
                           'wallets': wjson})
        except AssertionError as e:
            res = jsonify({'result': 'false', 'message': str(e)})
            res.status_code = 400

        return res


@app.before_request
def log_request_info():
    app.logger.info('Headers: %s', request.headers)
    app.logger.info('Body: %s', request.get_data())
    app.logger.info('Url: %s', request.url)
    app.logger.info('Data: %s', request.data)
    app.logger.info('Args: %s', request.args.__dict__)


if __name__ == '__main__':
    app.run(host=os.getenv('SERVER_HOST'),
            port=os.getenv('SERVER_PORT'),
            debug=os.getenv('SERVER_DEBUG'))
