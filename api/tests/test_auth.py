import unittest
import requests


class ApiTestAuthKey(unittest.TestCase):

    def test_normal(self):

        headers = {"Authorization": "Bearer MerchantBearerToken"}

        end_point = 'https://x-2.io/api/pub/v1/auth/new'

        r = requests.post(end_point, json={
                "email": "my@email.com",
                "description": "A new customer to access API",
                "pwd": "NewCustomer Password",
                "pwd_c": "NewCustomer Password"
        }, headers=headers, verify=False)

        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.json['status'], 'Success')

        print(r.json['key'])

        # {'status':'Success', 'key': 'ThnJA0fD5Ie8Rugt541plmbjmcm8caVW'}
