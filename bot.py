from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import CallbackQueryHandler
from telegram.ext import RegexHandler
from telegram import InlineKeyboardMarkup
import storage
import messages as mes
import processors as proc
import menu
import logging
import pprint
import wallet.storage as w_db
from dotenv import load_dotenv
import os
from content.langs import Message


load_dotenv()

TOKEN = os.getenv('BOT_TOKEN')

JOB_PREFIX = "N"
ALL_TOKENS = "ALL"
DEFAULT_THRESHOLD_PERCENTS = 5
DEFUALT_PERCENT_CHANGE_PERIOD = "percent_change_1h"
BOTANIO_API_TOKEN = "2c39ce04-e93d-4b06-98ee-ed6107380bf4"
RUN_INTERVAL = 60

GLOBAL_JOB_DICT = {}

logging.basicConfig(
                    format=mes.LogFormat,
                    level=logging.INFO)

logger = logging.getLogger(__name__)


def error_handler(bot, update, error):
    logger.warning('Update "%s" caused error "%s"', update, error)


def menu_tap_callback(bot, update, job_queue, chat_data):

    cmd = update.callback_query.data
    proc.proc_menu_tap(cmd, bot, update, job_queue, chat_data)


def choose_lang(bot, update, args, job_queue, chat_data):
    reply_markup = InlineKeyboardMarkup([menu.get_lang_menu()])
    bot.send_message(chat_id=update.message.chat_id,
                     text="Please choose your language first.",
                     parse_mode='HTML', reply_markup=reply_markup)


def start(bot, update, args, job_queue, chat_data):
    lang = storage.get_botlang(str(update.message.chat_id))
    if (not lang):
        choose_lang(bot, update, args, job_queue, chat_data=chat_data)
    else:
        chat_data['loc'] = Message(str(update.message.chat_id), lang=lang.Lang)
        proc.proc_welcome(bot, update, args, job_queue, chat_data=chat_data)


# gets tick by symbol name, need to be refactored
# (replace with storage.get_tick_by_token)
def get_tick_by_symbol(ticks, symb):

    for tick in ticks:
        if (symb == tick["symbol"]):
            return tick

    return None


def send_top_5_tokens(bot, chat_id, ticks):

        txt = ''
        i = 0
        while i < 6:
            txt += mes.get_coin_short_info(
                "{0} ({1})".format(ticks["tick"][i]["name"],
                                   ticks["tick"][i]["symbol"]),
                ticks["tick"][i]["price_usd"],
                ticks["tick"][i]["percent_change_24h"]
                                  )+"\n"
            i = i + 1

        bot.send_message(chat_id=chat_id, text=txt,
                         parse_mode='HTML')


def send_detailed_token(bot, update, ticks, token_symbol):

        t = get_tick_by_symbol(ticks["tick"], token_symbol)

        if (t is not None):
            txt = mes.get_detailed_info(t["name"], t["symbol"],
                                        float(t["price_usd"]),
                                        float(t["price_btc"]),
                                        float(t["percent_change_7d"]),
                                        float(t["percent_change_24h"]),
                                        float(t["percent_change_1h"]),
                                        float(t["market_cap_usd"]),
                                        float(t["total_supply"]))
            bot.send_message(chat_id=update.message.chat_id, text=txt,
                             parse_mode='HTML')


def get(bot, update, args):

    ticks = storage.get_ticks()

    symb = ALL_TOKENS

    if (len(args) == 1):
        symb = args[0].upper()

    if symb == ALL_TOKENS:
        send_top_5_tokens(bot, update.message.chat_id, ticks)
    else:
        send_detailed_token(bot, update, ticks, symb)


def calc_price_change(before, now):

    d = now - before
    print(d)
    change = (d/now)*100 if (d > 0) else (d/before)*100
    return {"change": change, "diff": d}


def notify_price_threshold(bot, job, t):

    n = job.context
    stamped_price = float(n.PriceUSD["price_usd"])

    d = n.ThreshHoldPrice - stamped_price

    upper_threshold = (d > 0)
    lower_threshold = (d < 0)

    tick = get_tick_by_symbol(t["tick"], n.Token)

    print ("stop loss",
           "{0}:{1}:{2}:{3}:{4}:ticker:{5}".format(d,
                                                   upper_threshold,
                                                   lower_threshold,
                                                   n.ThreshHoldPrice,
                                                   stamped_price,
                                                   tick["price_usd"]))

    if (tick is not None) and (
                (upper_threshold
                    and float(tick["price_usd"]) > n.ThreshHoldPrice) or
                (lower_threshold
                    and float(tick["price_usd"]) < n.ThreshHoldPrice)):

                txt = mes.get_stop_loss_change_info(
                                            tick["name"],
                                            float(n.PriceUSD["price_usd"]),
                                            float(tick["price_usd"]),
                                            float(n.ThreshHoldPrice))

                # removing triggered stop loss notification
                storage.remove_notification_by_id(n.UserId, n.Id)

                del GLOBAL_JOB_DICT[n.Id]

                # add new stop loss with current token price
                id = storage.save_notifications(n.UserId,
                                                n.Token, 0,
                                                n.ThreshHoldPrice,
                                                tick)

                n_new = storage.get_notification(n.UserId, id)

                job.context = n_new
                GLOBAL_JOB_DICT[n_new.Id] = job

                # debug console message
                pprint.pprint(
                    "Sending message {0}: to {1}".format(txt,
                                                         n_new.UserId))

                bot.send_message(chat_id=n_new.UserId,
                                 text=txt, parse_mode='HTML')


def notify_percent_threshold(bot, job, t):

    n = job.context

    tick = get_tick_by_symbol(t["tick"], n.Token)

    if (tick is not None):

        price_range = calc_price_change(float(n.PriceUSD["price_usd"]),
                                        float(tick["price_usd"]))

        # print("Executing job ... ")

        print("{0}:{1}:{2}:{3}".format(abs(price_range["change"]),
                                       n.ThresholdPercent,
                                       n.Token,
                                       tick["symbol"]))

        if abs(price_range["change"]) > float(n.ThresholdPercent):
                txt = mes.get_price_change_info(
                            tick["name"],
                            float(n.PriceUSD["price_usd"]),
                            float(tick["price_usd"]),
                            float(price_range["diff"]),
                            float(price_range["change"])
                          )

                # removing triggered stop loss notification
                storage.remove_notification_by_id(n.UserId, n.Id)
                del GLOBAL_JOB_DICT[n.Id]

                id = storage.save_notifications(n.UserId,
                                                tick["symbol"],
                                                n.ThresholdPercent, 0,
                                                tick)

                n_new = storage.get_notification(n.UserId, id)
                job.context = n_new
                GLOBAL_JOB_DICT[n_new.Id] = job

                pprint.pprint("Sending message {0}: to {1}".format(txt,
                              n_new.UserId))

                bot.send_message(chat_id=n_new.UserId, text=txt,
                                 parse_mode='HTML')


def callback_change(bot, job):

    t = storage.get_ticks()
    n = job.context

    if (n.ThreshHoldPrice == 0):
        notify_percent_threshold(bot, job, t)
    else:
        notify_price_threshold(bot, job, t)


def parse_notification_args(args):

    symb = ALL_TOKENS
    threshold = DEFAULT_THRESHOLD_PERCENTS

    args_size = len(args)

    if (args_size == 2):
        symb = args[0].upper()
        threshold = float(args[1])
    else:
        raise ValueError("Arguments are not consistent, can't parse.")

    return {"symbol": symb, "threshold": threshold}


def parse_stoploss_args(args):

    symb = ALL_TOKENS
    threshold = DEFAULT_THRESHOLD_PERCENTS

    args_size = len(args)

    if (args_size == 2):
        symb = args[0].upper()
        threshold = float(args[1])
    else:
        raise ValueError("Arguments are not consistent, can't parse.")

    return {"symbol": symb, "threshold": threshold}


def parse_portfolio_params(args):

    if (len(args) == 0):
        raise ValueError("Arguments are not consistent, can't parse.")

    cmd = args[0]

    if (len(args) == 5):
        return {"command": cmd, "amount": args[1],
                "token": args[2], "price": args[3], "token_change": args[4]}

    return {"command": cmd}


def add_stoploss(bot, update, args, job_queue, chat_data):

    params = parse_notification_args(args)

    tick = get_tick_by_symbol(storage.get_ticks()["tick"], params["symbol"])

    id = storage.save_notifications(update.message.chat_id,
                                    params["symbol"], 0,
                                    params["threshold"],
                                    tick)

    n = storage.get_notification(update.message.chat_id, id)

    bot.send_message(chat_id=update.message.chat_id,
                     text=mes.NotificationSetting)

    job = job_queue.run_repeating(callback_change, interval=RUN_INTERVAL,
                                  first=0, context=n)

    GLOBAL_JOB_DICT[n.Id] = job

    bot.send_message(chat_id=update.message.chat_id,
                     text=mes.NotificationAdded)


# Clear percent notifications/remove all token from job list
def clear_percent_notification(token, user_id):

    notifications = storage.get_percent_notification(token, user_id)

    for n in notifications:
        job = GLOBAL_JOB_DICT[n.Id]
        job.schedule_removal()
        del GLOBAL_JOB_DICT[n.Id]


def add_wallet_notification(bot, update, args, job_queue, chat_data):

    job = job_queue.run_repeating(proc.callback_wallet_change,
                                  interval=proc.RUN_INTERVAL_WALLET,
                                  first=0, context=update.message.chat_id)
    proc.WALLET_JOB_DICT[update.message.chat_id] = job


def add_notification(bot, update, args, job_queue, chat_data):

    params = parse_notification_args(args)

    tick = get_tick_by_symbol(storage.get_ticks()["tick"], params["symbol"])

    clear_percent_notification(update.message.chat_id, params["symbol"])
    storage.remove_percent_notifications(update.message.chat_id,
                                         params["symbol"])

    id = storage.save_notifications(update.message.chat_id,
                                    params["symbol"],
                                    params["threshold"],
                                    0,
                                    tick)

    n = storage.get_notification(update.message.chat_id, id)

    bot.send_message(chat_id=update.message.chat_id,
                     text=mes.NotificationSetting)

    job = job_queue.run_repeating(callback_change, interval=RUN_INTERVAL,
                                  first=0, context=n)

    GLOBAL_JOB_DICT[n.Id] = job

    bot.send_message(chat_id=update.message.chat_id,
                     text=mes.NotificationAdded)


def list_notifications(bot, update, chat_data):

    vals = []

    notifications = storage.get_notifications(update.message.chat_id, "")

    for n in notifications:
        vals.append(mes.get_notification_friendly_name(n))

    bot.send_message(chat_id=update.message.chat_id,
                     text=mes.get_notification_list_info(vals),
                     parse_mode='HTML')


def remove_notifications(bot, update, args, chat_data):

    vals = []

    symb = ""

    if (len(args) == 1):
        symb = args[0].upper()

    notifications = storage.get_notifications(update.message.chat_id, symb)

    for n in notifications:
        job = GLOBAL_JOB_DICT[n.Id]
        job.schedule_removal()
        del GLOBAL_JOB_DICT[n.Id]
        vals.append(mes.get_notification_friendly_name(n))

    storage.remove_notifications(update.message.chat_id, symb)

    bot.send_message(chat_id=update.message.chat_id,
                     text=mes.get_notification_list_removal(vals),
                     parse_mode='HTML')


def echo(bot, update):

    chat = bot.getChat(update.message.chat_id)
    txt = "This chat echo: {0}; {1}; {2}".format(chat.id, chat.type,
                                                 update.message.from_user.id)

    bot.send_message(chat_id=update.message.chat_id, text=txt)


def isFiatToken(token):
    return token.upper() == 'USD'


def _add_fiat_exchange(user_id, params):

    storage.save_buy_portfolio_record(user_id,
                                      params["token"],
                                      float(params["amount"]),
                                      float(params["price"]),
                                      params["token_change"])


def _add_sell_crypto(user_id, params):

    # getting avrg price for both tokens

    t = storage.get_records_totals_by_token(user_id, params["token"])
    te = storage.get_records_totals_by_token(user_id, params["token_change"])

    if ((t is not None) and (te is not None)):

        # burning tokens for sale, price = 0 (not to affects avr price in usd)

        storage.save_sell_portfolio_record(user_id,
                                           params["token"],
                                           float(params["amount"]),
                                           t.AvrgPrice,
                                           params["token_change"])

        # adding tokens to buy, price = 0 (not to affects avr price in usd)

        storage.save_buy_portfolio_record(user_id,
                                          params["token_change"],
                                          float(params["price"]),
                                          te.AvrgPrice,
                                          params["token"])


def _add_buy_crypto(user_id, params):

    # getting avrg price for both tokens

    t = storage.get_records_totals_by_token(user_id, params["token"])
    te = storage.get_records_totals_by_token(user_id, params["token_change"])

    # burning tokens for sale

    if ((t is not None) and (te is not None)):

        storage.save_buy_portfolio_record(user_id,
                                          params["token"],
                                          float(params["amount"]),
                                          t.AvrgPrice,
                                          params["token_change"])

        # adding tokens to buy

        storage.save_sell_portfolio_record(user_id,
                                           params["token_change"],
                                           float(params["price"]),
                                           te.AvrgPrice,
                                           params["token"])


def add_buy_portfolio(bot, update, params):

    if isFiatToken(params["token_change"]):
        _add_fiat_exchange(update.message.chat_id, params)
    else:
        _add_buy_crypto(update.message.chat_id, params)

    bot.send_message(chat_id=update.message.chat_id,
                     text=mes.PortfolioUpdated)

    list_portfolio(bot, update, params)


def add_sell_portfolio(bot, update, params):

    if isFiatToken(params["token"]):
        storage.save_sell_portfolio_record(update.message.chat_id,
                                           params["token"],
                                           float(params["amount"]),
                                           float(params["price"]),
                                           params["token_change"])
    else:
        _add_sell_crypto(update.message.chat_id, params)

    bot.send_message(chat_id=update.message.chat_id,
                     text=mes.PortfolioUpdated)

    list_portfolio(bot, update, params)


def list_portfolio(bot, update, params):

    vals = []
    totals = storage.get_records_totals_by_user(update.message.chat_id)

    ticks = storage.get_ticks()["tick"]

    for t in totals:
        aggr_token = storage.aggr_portfolio_token_totals(ticks, t)
        vals.append(mes.get_totals_friendly_name(t, aggr_token))

    aggr = storage.aggr_portfolio_totals(ticks, totals)

    bot.send_message(chat_id=update.message.chat_id,
                     text=mes.get_totals_list_info(vals, aggr),
                     parse_mode='HTML')


def add_portfolio(bot, update, args, job_queue, chat_data):

    params = parse_portfolio_params(args)

    if (params["command"].upper() == "BUY"):
        add_buy_portfolio(bot, update, params)

    elif (params["command"].upper() == "SELL"):
        add_sell_portfolio(bot, update, params)

    elif (params["command"].upper() == "LIST"):
        list_portfolio(bot, update, params)


def proc_ask(bot, update, args, job_queue, chat_data):

    proc.proc_exchange(bot, update, args, job_queue, chat_data, type=1)


def proc_bid(bot, update, args, job_queue, chat_data):

    proc.proc_exchange(bot, update, args, job_queue, chat_data, type=0)


def init_jobs_from_db(jq):
    notifications = storage.get_notifications("", "")

    # print(len(notifications))

    for n in notifications:
        # pprint.pprint(n)
        job = jq.run_repeating(callback_change, interval=RUN_INTERVAL,
                               first=0, context=n)
        GLOBAL_JOB_DICT[n.Id] = job

    # wallet notifications
    uids = w_db.get_uids()
    for uid in uids:
        job = jq.run_repeating(proc.callback_wallet_change,
                               interval=proc.RUN_INTERVAL_WALLET,
                               first=0, context=uid)
        proc.WALLET_JOB_DICT[uid] = job


def main():

    updater = Updater(token=TOKEN)

    j = updater.job_queue

    dsp = updater.dispatcher

    start_handler = CommandHandler('start', start,
                                   pass_args=True,
                                   pass_job_queue=True,
                                   pass_chat_data=True)
    echo_handler = CommandHandler('echo', echo)
    get_handler = CommandHandler('get', get, pass_args=True)
    notify_handler = CommandHandler('notify', add_notification,
                                    pass_args=True,
                                    pass_job_queue=True,
                                    pass_chat_data=True)

    notify_wallet_handler = CommandHandler('notify_wallet',
                                           add_wallet_notification,
                                           pass_args=True,
                                           pass_job_queue=True,
                                           pass_chat_data=True)

    stoploss_handler = CommandHandler('check', add_stoploss,
                                      pass_args=True,
                                      pass_job_queue=True,
                                      pass_chat_data=True)

    notification_list_handler = CommandHandler('list', list_notifications,
                                               pass_chat_data=True)

    notification_remove_handler = CommandHandler('remove',
                                                 remove_notifications,
                                                 pass_args=True,
                                                 pass_chat_data=True)

    portfolio_handler = CommandHandler('book', add_portfolio,
                                       pass_args=True,
                                       pass_job_queue=True,
                                       pass_chat_data=True)

    wallet_handler = CommandHandler('wallet', proc.proc_wallet,
                                    pass_args=True,
                                    pass_job_queue=True,
                                    pass_chat_data=True)

    ask_handler = CommandHandler('buy', proc_ask,
                                 pass_args=True,
                                 pass_job_queue=True,
                                 pass_chat_data=True)

    bid_handler = CommandHandler('sell', proc_bid,
                                 pass_args=True,
                                 pass_job_queue=True,
                                 pass_chat_data=True)

    lang_handler = CommandHandler('lang', choose_lang,
                                  pass_args=True,
                                  pass_job_queue=True,
                                  pass_chat_data=True)

    callback_handler = CallbackQueryHandler(menu_tap_callback,
                                            pass_job_queue=True,
                                            pass_chat_data=True)

    fl_mes_handler = RegexHandler('^-?\d*(\.\d+)?$', proc.proc_float_input,
                                  pass_chat_data=True)

    addr_mes_handler = RegexHandler(
        '\S*(\S*([a-zA-Z]\S*[0-9])|([0-9]\S*[a-zA-Z]))\S*',
        proc.proc_address_input_withdraw, pass_chat_data=True)

    dsp.add_handler(start_handler)
    dsp.add_handler(get_handler)
    dsp.add_handler(notify_handler)
    dsp.add_handler(notify_wallet_handler)
    dsp.add_handler(notification_list_handler)
    dsp.add_handler(notification_remove_handler)
    dsp.add_handler(echo_handler)
    dsp.add_handler(stoploss_handler)
    dsp.add_handler(lang_handler)

    # portfolio management
    dsp.add_handler(portfolio_handler)

    # wallet operation routine
    dsp.add_handler(wallet_handler)

    # exchange handlers
    dsp.add_handler(ask_handler)
    dsp.add_handler(bid_handler)

    # menu callback
    dsp.add_handler(callback_handler)
    dsp.add_handler(fl_mes_handler)
    dsp.add_handler(addr_mes_handler)
    dsp.add_error_handler(error_handler)

    init_jobs_from_db(j)

    updater.start_polling()

    updater.idle()


if __name__ == '__main__':
    main()
