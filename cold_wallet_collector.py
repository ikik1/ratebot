import wallet.config as cfg
import wallet.storage as db
from wallet.operations import WalletOps
from wallet.operations import BtcOps, EthOps
from dotenv import load_dotenv
import os
from decimal import Decimal
import logging

logging.basicConfig(level=logging.INFO,
                    format='[%(levelname)s] (%(threadName)-10s) %(message)s',
                    )

load_dotenv()


def deduct_collection_limit(amount, limit):
    return amount - (limit/100)*amount


# Collecting BTC

def collect_btc():

    print('Collecting BTC ...')

    coin = cfg.BC_COIN

    ops = db.get_top_ups(coin, collected=False)

    for o in ops:
        print(o)
        try:
            w = db.get_wallet_by_uid(o.UserId, o.Coin)
            cold_addr = os.getenv("COLD_BTC_ADDR")

            if w and cold_addr:

                wo = WalletOps(coin=w.Coin, addr=w.Address)
                btc_ops = BtcOps(coin=w.Coin, uid=w.UserId)
                amount = deduct_collection_limit(
                    wo.process_amount_from_normal_dec(o.CreditAmount),
                    Decimal(cfg.BC_COLLECTION_LIMIT))

                tx = btc_ops.to_cold_address(w.Address, cold_addr,
                                             w.PrivateKey, amount)

                assert tx is not None, 'Can not collect %r' % o.Id

                wo.roll(db.OpTypes.Cold, amount, reference=o.Id,
                        callback=None)

                print("""Amount of %r %r collected from %r
                         topup operation at tx %r"""
                      % (amount, w.Coin, o.Id, tx))
        except Exception as e:
            logging.exception(e)


# Collecting ETH

def collect_eth():

    print('Collecting ETH ...')

    coin = cfg.ETH_COIN

    ops = db.get_top_ups(coin, collected=False)

    for o in ops:
        try:
            print(o)
            w = db.get_wallet_by_uid(o.UserId, o.Coin)
            cold_addr = os.getenv("COLD_ETH_ADDR")

            if w and cold_addr:

                wo = WalletOps(coin=w.Coin, addr=w.Address)
                btc_ops = EthOps(coin=w.Coin, uid=w.UserId)
                amount = deduct_collection_limit(
                    wo.process_amount_from_normal_dec(o.CreditAmount),
                    Decimal(cfg.ETH_COLLECTION_LIMIT))

                tx = btc_ops.to_cold_address(w.Address, cold_addr,
                                             w.PrivateKey, amount)

                assert tx is not None, 'Can not collect %r' % o.Id

                wo.roll(db.OpTypes.Cold, amount, reference=o.Id,
                        callback=None)

                print("""Amount of %r %r collected from %r
                         topup operation at tx %r"""
                      % (amount, w.Coin, o.Id, tx))
        except Exception as e:
            logging.exception(e)


# Collecting ERC20

def collect_erc20(token):

    print('Collecting ERC20 ...')

    coin = token

    ops = db.get_top_ups(coin, collected=False)

    for o in ops:
        try:
            print(o)
            w = db.get_wallet_by_uid(o.UserId, o.Coin)
            cold_addr = os.getenv("COLD_ETH_ADDR")

            if w and cold_addr:

                wo = WalletOps(coin=w.Coin, addr=w.Address)
                btc_ops = EthOps(coin=w.Coin, uid=w.UserId)
                amount = deduct_collection_limit(
                    wo.process_amount_from_normal_dec(o.CreditAmount),
                    Decimal(cfg.ERC20_COLLECTION_LIMIT))

                tx = btc_ops.to_cold_address_erc20(w.Coin, w.Address,
                                                   cold_addr, w.PrivateKey,
                                                   amount)

                assert tx is not None, 'Can not collect %r' % o.Id

                wo.roll(db.OpTypes.Cold, amount, reference=o.Id,
                        callback=None)

                print("""Amount of %r %r collected from %r
                        topup operation at tx %r"""
                      % (amount, w.Coin, o.Id, tx))
        except Exception as e:
            logging.exception(e)


collect_btc()
collect_erc20('gvc')
collect_eth()
