from blockcypher import unsubscribe_from_webhook
import wallet.storage as db
import wallet.operations as ops
import wallet.config as cfg

uids = db.get_uids()


class HookedOps(ops.BtcOps):

    def get_hook(self, addr):
        return super()._top_up_hook(addr)


for uid in uids:

    btc_ops = HookedOps(cfg.BC_COIN, str(uid))
    w = btc_ops.get_wallet()
    print(unsubscribe_from_webhook('bcaf7c39-9a7f-4e8b-8ba4-23b3c1806039',
                                   api_key=cfg.BC_TOKEN))
    hook = btc_ops.get_hook(w.Address)
    db.update_wallet_hook(w, hook)
    print(hook)
