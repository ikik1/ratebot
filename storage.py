import pymongo
from pymongo import MongoClient
import datetime
import wallet.config as cfg

CN = cfg.DB_CONNECTION
client = MongoClient(CN)
db = client['cmcDB']


class BotLang():
    Id = ''
    CreatedAt = ''
    Lang = 'en'
    UserId = ''


# Class for notification mapping from mongoDB, should be placed into /entites
class Notification():

    Id = ''
    CreatedAt = ''
    UserId = ''
    PriceUSD = ''
    ThresholdPercent = 0.0
    ThreshHoldPrice = 0.0
    Token = ''
    IsDeleted = 0


# Class for portfolio record storing
class PortfolioRecord():

    Id = ''
    CreatedAt = ''
    UserId = ''
    Token = ''
    TokenAmount = 0
    Price = 0
    ExchangeToken = ''
    IsDeleted = 0


# Class for portfolio total storing
class PortfolioTotals():

    Id = ''
    CreatedAt = ''
    UserId = ''
    Token = ''
    TotalAmount = 0.0
    AvrgPrice = 0.0
    TotalVolume = 0.0
    IsDeleted = 0


def get_col_botlang():

    return db['botlang']


# gets notifications collection from db using string name
def get_col_notifications():

    return db['notifications']


# gets portfolio_totals collection from db using string name
def get_col_portfolio_totals():

    return db['portfolio_totals']


# gets portfolio_records collection
def get_col_portfolio_records():

    return db['portfolio_records']


# gets ticks collection by name
def get_col_ticks():

    return db['ticks']


def map_botlang(dict):

    botlang_record = BotLang()
    botlang_record.Id = dict["_id"]
    botlang_record.CreatedAt = dict["CreatedAt"]
    botlang_record.UserId = dict["UserId"]
    botlang_record.Lang = dict["Lang"]

    return botlang_record


# Simple object mapping, should be refactored using MongoEngine or MongoKit
def map_portfolio_record(dict):

    portfolio_record = PortfolioRecord()
    portfolio_record.Id = dict["_id"]
    portfolio_record.CreatedAt = dict["CreatedAt"]
    portfolio_record.UserId = dict["UserId"]
    portfolio_record.Token = dict["Token"]
    portfolio_record.TokenAmount = dict["TokenAmount"]
    portfolio_record.Price = dict["Price"]
    portfolio_record.ExchangeToken = dict["ExchangeToken"]
    portfolio_record.IsDeleted = dict["IsDeleted"]

    return portfolio_record


# Simple object mapping, should be refactored using MongoEngine or MongoKit
def map_portfolio_totals(dict):

    portfolio_totals = PortfolioTotals()
    portfolio_totals.Id = dict["_id"]
    portfolio_totals.CreatedAt = dict["CreatedAt"]
    portfolio_totals.UserId = dict["UserId"]
    portfolio_totals.Token = dict["Token"]
    portfolio_totals.TotalAmount = dict["TotalAmount"]
    portfolio_totals.AvrgPrice = dict["AvrgPrice"]
    portfolio_totals.TotalVolume = dict["TotalVolume"]
    portfolio_totals.IsDeleted = dict["IsDeleted"]

    return portfolio_totals


# Simple object mapping, should be refactored using MongoEngine or MongoKit
def map_notification(dict):

    # pprint.pprint(dict)

    notification = Notification()
    notification.Id = dict["_id"]
    notification.CreatedAt = dict["CreatedAt"]
    notification.UserId = dict["UserId"]
    notification.PriceUSD = dict["PriceUSD"]
    notification.ThresholdPercent = dict["ThresholdPercent"]
    notification.ThreshHoldPrice = dict["ThreshHoldPrice"]
    notification.Token = dict["Token"]
    notification.IsDeleted = dict["IsDeleted"]
    return notification


# Maps many objects of Notification type
def map_many_notifications(dict_arr):

    res = []
    for d in dict_arr:
        # debug pprint.pprint(d)
        res.append(map_notification(d))
    return res


# Maps many objects of PortfolioRecord type
def map_many_portfolio_records(dict_arr):

    res = []
    for d in dict_arr:
        # debug pprint.pprint(d)
        res.append(map_portfolio_record(d))
    return res


# Maps many objects of PortfolioRecord type
def map_many_portfolio_totals(dict_arr):

    res = []
    for d in dict_arr:
        # debug pprint.pprint(d)
        res.append(map_portfolio_totals(d))
    return res


# All dynamic clauses shall be refactored to $expr dynamic operator
def get_empty_token_clause(user_id, token):

    if (token == "" and user_id != ""):
            return {"UserId": user_id, "IsDeleted": 0}

    if (user_id == "" and token == ""):
            return {"IsDeleted": 0}

    if (token != "" and user_id == ""):
            return {"Token": token, "IsDeleted": 0}

    else:
            return {"UserId": user_id, "Token": token, "IsDeleted": 0}


def get_id_clause(user_id, id):

    return {"UserId": user_id, "IsDeleted": 0, "_id": id}


def get_uid_clause(user_id):

    return {"UserId": user_id}


# All dynamic clauses shall be refactored to $expr dynamic operator
def get_none_stoploss_clause(user_id, token):

    res = {"ThreshHoldPrice": 0, "UserId": user_id, "Token": token,
           "IsDeleted": 0}

    return res


def save_ticks(ticks):

    tick_post = {"date": datetime.datetime.utcnow(), "tick": ticks}
    ticks = get_col_ticks()
    ticks.delete_many({})
    _id = ticks.insert_one(tick_post).inserted_id

    return _id


def get_ticks():

    ticks = get_col_ticks()
    return ticks.find_one()


def get_tick_by_token(ticks, token):

    for tick in ticks:
        if (token.upper() == tick["symbol"].upper()):
            return tick

    return None


def get_botlang(uid):

    col = get_col_botlang()
    cur = col.find_one(get_uid_clause(uid))
    if cur:
        return map_botlang(cur)
    else:
        return None


def save_botlang(user_id, lang=None):

    bl = get_botlang(user_id)

    col = get_col_botlang()

    if (not bl):
        bl = BotLang()
        bl.CreatedAt = datetime.datetime.utcnow()
        bl.UserId = str(user_id)
        bl.Lang = lang
        col.insert_one(bl.__dict__).inserted_id
    elif (lang != bl.Lang):
        col.update_one({'UserId': user_id}, {'$set': {'Lang': lang}})

    return map_botlang(col.find_one(get_uid_clause(user_id)))


def save_notifications(user_id, token, threshold_percent,
                       stop_loss, price_usd):

    notification = Notification()
    notification.CreatedAt = datetime.datetime.utcnow()
    notification.UserId = user_id
    notification.PriceUSD = price_usd
    notification.ThresholdPercent = threshold_percent
    notification.ThreshHoldPrice = stop_loss
    notification.Token = token
    notification.IsDeleted = 0

    col_notifications = get_col_notifications()
    _id = col_notifications.insert_one(notification.__dict__).inserted_id

    return _id


def _save_portfolio_record(user_id, token, amount, price, exchange_token):

    portfolio_record = PortfolioRecord()
    portfolio_record.CreatedAt = datetime.datetime.utcnow()
    portfolio_record.UserId = user_id
    portfolio_record.Token = token
    portfolio_record.TokenAmount = amount
    portfolio_record.Price = price
    portfolio_record.ExchangeToken = exchange_token
    portfolio_record.IsDeleted = 0

    col_pr = get_col_portfolio_records()
    _id = col_pr.insert_one(portfolio_record.__dict__).inserted_id

    _update_record_totals(user_id, token, amount, price)

    return _id


def save_buy_portfolio_record(user_id, token, amount, price, token_exchange):

    buy_amount = abs(amount)
    return _save_portfolio_record(user_id, token, buy_amount,
                                  price, token_exchange)


def save_sell_portfolio_record(user_id, token, amount, price, token_exchange):

    sell_amount = abs(amount)*(-1)
    return _save_portfolio_record(user_id, token, sell_amount,
                                  price, token_exchange)


def _update_record_totals(user_id, token, amount, price):

    totals = get_records_totals_by_token(user_id, token)
    prev_amount = 0.0
    prev_volume = 0.0

    if totals is not None:
        remove_totals_by_id(user_id, totals.Id)
        prev_amount = totals.TotalAmount
        prev_volume = totals.TotalVolume

    new_totals = PortfolioTotals()
    new_totals.CreatedAt = datetime.datetime.utcnow()
    new_totals.UserId = user_id
    new_totals.Token = token

    new_totals.TotalAmount = prev_amount+amount
    new_totals.TotalVolume = prev_volume+(amount*price)
    new_totals.AvrgPrice = new_totals.TotalVolume/new_totals.TotalAmount
    new_totals.IsDeleted = 0

    col_pt = get_col_portfolio_totals()
    _id = col_pt.insert_one(new_totals.__dict__).inserted_id

    return _id


def get_records_totals_by_token(user_id, token):

    col_totals = get_col_portfolio_totals()
    clause = get_empty_token_clause(user_id, token)
    r = col_totals.find_one(clause)

    if r is not None:
        return map_portfolio_totals(r)
    else:
        return r


def get_records_totals_by_user(user_id):

    col_totals = get_col_portfolio_totals()
    clause = get_empty_token_clause(user_id, "")

    print(clause)

    cur = col_totals.find(clause).sort([("Token", pymongo.ASCENDING)])

    return map_many_portfolio_totals(cur)


def get_notifications(user_id="", token=""):

    col_notification = get_col_notifications()
    clause = get_empty_token_clause(user_id, token)
    cur = col_notification.find(clause).sort([("Token", pymongo.ASCENDING)])
    return map_many_notifications(cur)


def get_percent_notification(user_id, token):

    col_notification = get_col_notifications()
    clause = get_none_stoploss_clause(user_id, token)
    cur = col_notification.find(clause)
    return map_many_notifications(cur)


def get_notification(user_id, id):

    col_notification = get_col_notifications()
    notification = col_notification.find_one(
                    get_id_clause(user_id, id))
    return map_notification(notification)


def get_growth_rate(current, previous):

    if previous == 0 or current == 0:
        return 0

    if (current > previous):
        return (current/previous)*100
    else:
        return (previous/current)*100*(-1)


def aggr_portfolio_token_totals(ticks, t):

    tick = get_tick_by_token(ticks, t.Token)
    current_volume = float(tick["price_usd"])*t.TotalAmount

    return {"CurrentVolume": current_volume,
            "Profit": current_volume-t.TotalVolume,
            "ChangeRate": get_growth_rate(current_volume, t.TotalVolume)}


def aggr_portfolio_totals(ticks, totals):

    total_volume = 0.0
    current_volume = 0.0

    for t in totals:
        total_volume += t.TotalVolume
        tick = get_tick_by_token(ticks, t.Token)
        current_volume += float(tick["price_usd"])*t.TotalAmount

    return {"TotalVolume": total_volume, "CurrentVolume": current_volume,
            "Profit": current_volume-total_volume,
            "ChangeRate": get_growth_rate(current_volume, total_volume)}


def remove_notifications(user_id, token):

    col_notification = get_col_notifications()
    res = col_notification.update_many(get_empty_token_clause(user_id, token),
                                       {"$set": {"IsDeleted": 1}})
    return res.modified_count


def remove_percent_notifications(user_id, token):

    col_notification = get_col_notifications()
    res = col_notification.update_many(
        get_none_stoploss_clause(user_id, token),
        {"$set": {"IsDeleted": 1}})

    return res.modified_count


def remove_notification_by_id(user_id, id):

    col_notification = get_col_notifications()
    res = col_notification.update_many(
                get_id_clause(user_id, id),
                {"$set": {"IsDeleted": 1}})

    return res.modified_count


def remove_totals_by_id(user_id, id):

    col_totals = get_col_portfolio_totals()
    res = col_totals.update_many(
                get_id_clause(user_id, id),
                {"$set": {"IsDeleted": 1}})

    return res.modified_count
