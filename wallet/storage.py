from pymongo import MongoClient
from enum import Enum
import datetime
from bson.objectid import ObjectId
from bson.decimal128 import Decimal128
import wallet.config as cfg
import pymongo


CN = cfg.DB_CONNECTION
client = MongoClient(CN)
db = client['cmcDB']


# Operation types
class OpTypes(Enum):

    Hold = 1
    Transfer = 2
    WriteOff = 3
    TopUp = 4
    Commission = 5
    Release = 6
    Cold = 7
    Withdraw = 8


# Class for storing customer wallet information
class Wallet():
    def __init__(self):
        self.Id = ''
        self.CreatedAt = ''
        self.UserId = ''
        self.Coin = ''
        self.BCinfo = None
        self.Address = ''
        self.PrivateKey = ''
        self.Balance = 0
        self.Hold = 0
        self.IsDeleted = 0
        self.TopUpHookId = ''


class Operation():

    Id = ''
    CreatedAt = ''
    UserId = ''
    WalletFrom = ''
    WalletTo = ''
    Type = 0
    CreditAmount = 0
    DebitAmount = 0
    Coin = ''
    Balance = 0
    Hold = 0
    InColdWallet = 0
    reference = ''


class HoldBalance():

    Id = ''
    CreatedAt = ''
    HoldId = ''
    Balance = ''


class EthTopUpRequest():

    Id = ''
    CreatedAt = ''
    Coin = ''
    To = ''
    Hash = ''
    Value = ''
    Cfms = ''


class EthWatchAddress:

    Id = ''
    CreatedAt = ''
    Address = ''


class OpNotification:

    Id = ''
    IsDeleted = 0
    CreatedAt = ''
    OpId = ''
    UserId = ''


class FiatPaymentRequest:

    Id = ''
    Amount = 0
    Coin = ''
    Cfms = 0
    UserId = ''
    Reference = ''
    CreatedAt = ''


# Simple object mapping, should be refactored using MongoEngine or MongoKit


def map_fiatpaymentrequest(dict):

    req = FiatPaymentRequest()
    req.Id = dict["_id"]
    req.CreatedAt = dict["CreatedAt"]
    req.Coin = dict["Coin"]
    req.Cfms = 0
    req.Amount = dict["Amount"].to_decimal()
    req.Reference = dict["Reference"]
    req.UserId = dict["UserId"]

    return req


def map_wallet(dict):

    wallet = Wallet()
    wallet.Id = dict["_id"]
    wallet.CreatedAt = dict["CreatedAt"]
    wallet.UserId = dict["UserId"]
    wallet.Coin = dict["Coin"]
    wallet.Address = dict["Address"]
    wallet.PrivateKey = dict["PrivateKey"]
    wallet.IsDeleted = dict["IsDeleted"]
    wallet.TopUpHookId = dict["TopUpHookId"]
    wallet.Balance = dict["Balance"].to_decimal()
    wallet.Hold = dict["Hold"].to_decimal()

    return wallet


def map_opnotification(dict):

    on = OpNotification()
    on.Id = dict["_id"]
    on.CreatedAt = dict["CreatedAt"]
    on.UserId = dict["UserId"]
    on.OpId = dict["OpId"]
    on.IsDeleted = dict["IsDeleted"]

    return on


def map_operation(dict):

    operation = Operation()
    operation.Id = dict["_id"]
    operation.CreatedAt = dict["CreatedAt"]
    operation.UserId = dict["UserId"]
    operation.WalletFrom = dict["WalletFrom"]
    operation.WalletTo = dict["WalletTo"]
    operation.Type = dict["Type"]
    operation.CreditAmount = dict["CreditAmount"].to_decimal()
    operation.DebitAmount = dict["DebitAmount"].to_decimal()
    operation.Coin = dict["Coin"]
    operation.Balance = dict["Balance"].to_decimal()
    operation.Hold = dict["Hold"].to_decimal()
    operation.InColdWallet = dict["InColdWallet"]
    operation.reference = dict["reference"]

    return operation


def map_EthTopUpRequest(dict):

    req = EthTopUpRequest()

    req.Id = dict["_id"]
    req.CreatedAt = dict["CreatedAt"]
    req.Coin = dict["Coin"]
    req.Hash = dict["Hash"]
    req.To = dict["To"]
    req.Value = dict["Value"]
    req.Cfms = dict["Cfms"]

    return req


def map_EthWatchAddress(dict):

    wa = EthWatchAddress()
    wa.Id = dict["_id"]
    wa.CreatedAt = dict["CreatedAt"]
    wa.Address = dict["Address"]

    return wa


def map_hold_balance(dict):
    hb = HoldBalance()
    hb.Id = dict["_id"]
    hb.CreatedAt = dict["CreatedAt"]
    hb.Balance = dict["Balance"].to_decimal()
    hb.HoldId = dict["HoldId"]

    return hb


# Maps many objects of Operation type
def map_many_operations(dict_arr):

    res = []
    for d in dict_arr:
        res.append(map_operation(d))
    return res


# Maps many objects of OpNotification type
def map_many_opnotifications(dict_arr):

    res = []
    for d in dict_arr:
        res.append(map_opnotification(d))
    return res


# Maps many objects of Wallet type
def map_many_wallets(dict_arr):

    res = []
    for d in dict_arr:
        res.append(map_wallet(d))
    return res


# gets wallet collection from db using string name
def get_col_wallets():
    return db['wallets']


# gets operation collection from db using string name
def get_col_ops():
    return db['operations']


def get_col_hold_balance():
    return db['holdbalances']


def get_col_ethwatchaddresses():
    return db['ethwatchaddresss']


def get_col_ethtopuprequest():
    return db["ethtopuprequest"]


def get_col_opnotification():
    return db["opnotifications"]


def get_col_fiatpaymentrequest():
    return db["fiatpaymentrequest"]


def get_wallet_uid_clause(uid, coin):
    return {"UserId": uid, "Coin": coin, "IsDeleted": 0}


def get_wallet_addr_clause(coin, addr):
    return {"Address": addr, "Coin": coin, "IsDeleted": 0}


def get_wallet_id_clause(id):
    return {"_id": id, "IsDeleted": 0}


def get_wallets_uid_clause(uid):
    return {"UserId": uid, "IsDeleted": 0}


def get_operation_id_clause(id):
    return {"_id": id}


def get_operation_ref_type_clause(ref, type):
    return {"reference": ref, "Type": type}


def get_opnotification_id_clause(id):
    return {"_id": id}


def get_opnotification_uid_clause(uid):
    return {"UserId": uid, "IsDeleted": 0}


def get_hold_balance_id_clause(id):
    return {"HoldId": id}


def get_fiatreq_id_clause(id):
    return {"_id": id}


def get_operation_wallets_clause(wallets):
    or_ = []
    for w in wallets:
        or_.append({"WalletFrom": ObjectId(w.Id)})
        or_.append({"WalletTo": ObjectId(w.Id)})

    return {"$or": or_}


def get_ethwathcaddress_clause(address):
    return {"Address": address}


def get_ethtopuprequest_hash_clause(hash):
    return {"Hash": hash}


def get_topup_not_collected_clause(coin):
    return {"Type": OpTypes.TopUp.value, "Coin": coin, "InColdWallet": 0}


def get_wallet_by_uid(uid, coin):
    col_wallets = get_col_wallets()
    clause = get_wallet_uid_clause(uid, coin)
    cur = col_wallets.find_one(clause)

    if (cur is not None):
        return map_wallet(cur)
    else:
        return cur


def get_wallet_by_address(coin, addr):
    col_wallets = get_col_wallets()
    clause = get_wallet_addr_clause(coin, addr)
    cur = col_wallets.find_one(clause)

    if (cur is not None):
        return map_wallet(cur)
    else:
        return cur


def get_wallets_by_uid(uid):

    col_wallets = get_col_wallets()
    clause = get_wallets_uid_clause(uid)

    cur = col_wallets.find(clause)

    return map_many_wallets(cur)


def get_op_by_id(op_id):

    col_ops = get_col_ops()
    clause = get_operation_id_clause(ObjectId(op_id))
    cur = col_ops.find_one(clause)

    if (cur is not None):
        return map_operation(cur)
    else:
        return cur


def get_wallet_by_opid(op_id):

    op = get_op_by_id(op_id)

    assert op is not None, "Operation not found"
    return get_wallet_by_uid(op.UserId, op.Coin)


def get_ethwatchaddress(addr):
    col_addrs = get_col_ethwatchaddresses()
    clause = get_ethwathcaddress_clause(addr)
    cur = col_addrs.find_one(clause)
    if (cur is not None):
        return map_EthWatchAddress(cur)
    else:
        return cur


def get_ethxttopup(hash):
    col_txs = get_col_ethtopuprequest()
    clause = get_ethtopuprequest_hash_clause(hash)
    cur = col_txs.find_one(clause)
    if (cur is not None):
        return map_EthTopUpRequest(cur)
    else:
        return cur


def new_wallet(uid, coin, address, pk, hook_id):

    assert uid is not None, "uid is not defined"
    assert coin is not None, "coin is not defined"
    assert address is not None, "address is not defined"
    assert pk is not None, "pk is not defined"

    w = Wallet()
    w.IsDeleted = 0
    w.UserId = str(uid)
    w.Coin = coin
    w.Address = address
    w.PrivateKey = pk
    w.CreatedAt = datetime.datetime.utcnow()
    w.TopUpHookId = hook_id
    w.Balance = Decimal128('0.0')
    w.Hold = Decimal128('0.0')

    col = get_col_wallets()
    _id = col.insert_one(w.__dict__).inserted_id

    return map_wallet(col.find_one(get_wallet_id_clause(_id)))


def clone_wallet_by_address(new_coin, clone_coin, address):

    w = get_wallet_by_address(clone_coin, address)

    if (w is None):
        return w

    return new_wallet(w.UserId, new_coin, w.Address, w.PrivateKey,
                      w.TopUpHookId)


def update_wallet_balance(wallet):

    col = get_col_wallets()
    col.update_one({'_id': wallet.Id},
                   {'$set': {'Balance': Decimal128(wallet.Balance),
                             'Hold': Decimal128(wallet.Hold)}})


def update_wallet_hook(wallet, hook):
    col = get_col_wallets()
    col.update_one({'_id': wallet.Id},
                   {'$set': {'TopUpHookId': hook}})


def __get_holdbalance_by_id(hid):

    col = get_col_hold_balance()
    clause = get_hold_balance_id_clause(hid)
    cur = col.find_one(clause)

    if (cur is not None):
        return map_hold_balance(cur)
    else:
        return cur


def check_hold_balance(hid, amount, update=True):

    hb = __get_holdbalance_by_id(hid)

    assert amount > 0, "Amount should be greater than zero"
    assert hb is not None, "Hold with id {0} not found".format(hid)
    assert hb.Balance-amount >= 0, \
        "Hold {0} balance is not enough: {1:f}, {2:f}".format(hid,
                                                              hb.Balance,
                                                              amount)

    if (update):
        hb.Balance = hb.Balance-amount
        col = get_col_hold_balance()
        col.update_one({'_id': hb.Id},
                       {'$set': {'Balance': Decimal128(hb.Balance)}})


def __add_hold_balance(hold):

    assert hold is not None, "Hold can't be null"

    h = HoldBalance()
    h.HoldId = str(hold.Id)
    h.Balance = Decimal128(hold.DebitAmount)
    h.CreatedAt = datetime.datetime.utcnow()

    col = get_col_hold_balance()
    col.insert_one(h.__dict__)


def add_hold_op(wallet, amount, reference):

    assert wallet is not None, "Wallet could not be null"
    assert amount > 0, "Ammount should be more than zero"

    o = Operation()
    o.CreatedAt = datetime.datetime.utcnow()
    o.UserId = wallet.UserId
    o.WalletFrom = wallet.Id
    o.WalletTo = wallet.Id
    o.Type = OpTypes.Hold.value
    o.CreditAmount = Decimal128('0')
    o.DebitAmount = Decimal128(amount)
    o.reference = reference
    o.Coin = wallet.Coin
    o.Balance = Decimal128(wallet.Balance)
    o.Hold = Decimal128(wallet.Hold)
    o.InColdWallet = 0

    col = get_col_ops()
    _id = col.insert_one(o.__dict__).inserted_id

    hb = map_operation(col.find_one(get_operation_id_clause(_id)))

    __add_hold_balance(hb)

    return hb


def add_transfer_op(wallet, target_wallet, amount, reference):

    assert wallet is not None, "Wallet can not be null"
    assert target_wallet is not None, "Target wallet can not be null"
    assert amount > 0, "Ammount should be more than zero"

    o = Operation()
    o.CreatedAt = datetime.datetime.utcnow()
    o.UserId = wallet.UserId
    o.WalletFrom = wallet.Id
    o.WalletTo = target_wallet.Id
    o.Type = OpTypes.Transfer.value
    o.reference = reference
    o.CreditAmount = Decimal128(amount)
    o.DebitAmount = Decimal128(amount)
    o.Coin = wallet.Coin
    o.Balance = Decimal128(wallet.Balance)
    o.Hold = Decimal128(wallet.Hold)
    o.InColdWallet = 0

    col = get_col_ops()
    _id = col.insert_one(o.__dict__).inserted_id
    return map_operation(col.find_one(get_operation_id_clause(_id)))


def add_writeoff_op(wallet, amount, reference):

    assert wallet is not None, "Wallet can not be null"
    assert amount > 0, "Ammount should be more than zero"

    o = Operation()
    o.CreatedAt = datetime.datetime.utcnow()
    o.UserId = wallet.UserId
    o.WalletFrom = wallet.Id
    o.WalletTo = wallet.Id
    o.Type = OpTypes.WriteOff.value
    o.reference = reference
    o.CreditAmount = Decimal128('0')
    o.DebitAmount = Decimal128(amount)
    o.Coin = wallet.Coin
    o.Balance = Decimal128(wallet.Balance)
    o.Hold = Decimal128(wallet.Hold)
    o.InColdWallet = 0

    col = get_col_ops()
    _id = col.insert_one(o.__dict__).inserted_id
    return map_operation(col.find_one(get_operation_id_clause(_id)))


def add_release_op(wallet, amount, reference):

    assert wallet is not None, "Wallet can not be null"
    assert amount > 0, "Ammount should be more than zero"

    o = Operation()
    o.CreatedAt = datetime.datetime.utcnow()
    o.UserId = wallet.UserId
    o.WalletFrom = wallet.Id
    o.WalletTo = wallet.Id
    o.Type = OpTypes.Release.value
    o.reference = reference
    o.CreditAmount = Decimal128('0')
    o.DebitAmount = Decimal128(amount)
    o.Coin = wallet.Coin
    o.Balance = Decimal128(wallet.Balance)
    o.Hold = Decimal128(wallet.Hold)
    o.InColdWallet = 0

    col = get_col_ops()
    _id = col.insert_one(o.__dict__).inserted_id
    return map_operation(col.find_one(get_operation_id_clause(_id)))


def _top_up_ref_exists(ref):

    col = get_col_ops()
    cnt = col.count(get_operation_ref_type_clause(ref, OpTypes.TopUp.value))
    return cnt > 0


def add_topup_op(wallet, amount, reference):

    assert wallet is not None, "Wallet can not be null"
    assert amount > 0, "Ammount should be more than zero"
    assert not _top_up_ref_exists(reference), "Topup ref exists"

    o = Operation()
    o.CreatedAt = datetime.datetime.utcnow()
    o.UserId = wallet.UserId
    o.WalletFrom = wallet.Id
    o.WalletTo = wallet.Id
    o.Type = OpTypes.TopUp.value
    o.reference = reference
    o.CreditAmount = Decimal128(amount)
    o.DebitAmount = Decimal128('0')
    o.Coin = wallet.Coin
    o.Balance = Decimal128(wallet.Balance)
    o.Hold = Decimal128(wallet.Hold)
    o.InColdWallet = 0

    col = get_col_ops()
    _id = col.insert_one(o.__dict__).inserted_id
    return map_operation(col.find_one(get_operation_id_clause(_id)))


def add_withdraw_op(wallet, amount, reference):

    assert wallet is not None, "Wallet can not be null"
    assert amount > 0, "Ammount should be more than zero"

    o = Operation()
    o.CreatedAt = datetime.datetime.utcnow()
    o.UserId = wallet.UserId
    o.WalletFrom = wallet.Id
    o.WalletTo = wallet.Id
    o.Type = OpTypes.Withdraw.value
    o.reference = reference
    o.CreditAmount = Decimal128('0')
    o.DebitAmount = Decimal128(amount)
    o.Coin = wallet.Coin
    o.Balance = Decimal128(wallet.Balance)
    o.Hold = Decimal128(wallet.Hold)
    o.InColdWallet = 0

    col = get_col_ops()
    _id = col.insert_one(o.__dict__).inserted_id
    return map_operation(col.find_one(get_operation_id_clause(_id)))


def add_EthWatchAddress(address):
    chk_addr = get_ethwatchaddress(address)
    if (chk_addr is None):

        a = EthWatchAddress()
        a.CreatedAt = datetime.datetime.utcnow()
        a.Address = address

        col = get_col_ethwatchaddresses()
        col.insert_one(a.__dict__)
        return map_EthWatchAddress(
                col.find_one(get_ethwathcaddress_clause(a.Address)))


def get_ethaddr_to_watch():
    col = get_col_ethwatchaddresses()
    cur = col.find({}, {'Address': 1})
    res = []
    for a in cur:
        res.append(a['Address'])
    print(res)
    return res


def add_EthTopUpRequest(hash, coin, to, value):

    assert hash is not None, "hash can not be null"
    assert coin is not None, "coin can not be null"
    assert to is not None, "to can not be null"
    assert value is not None, "value can not be null"

    tx_check = get_ethxttopup(hash)

    if (tx_check is None):

        req = EthTopUpRequest()
        req.CreatedAt = datetime.datetime.utcnow()
        req.Coin = coin
        req.Hash = hash
        req.To = to
        req.Value = value
        req.Cfms = 0

        col = get_col_ethtopuprequest()
        col.insert_one(req.__dict__)

        return get_ethxttopup(req.Hash)


def add_FiatPaymentRequest(uid, amount, coin):

    assert coin is not None, "coin can not be null"
    assert amount > 0, "amount can't be zero value"
    assert uid is not None, "uid can't be None"

    req = FiatPaymentRequest()
    req.CreatedAt = datetime.datetime.utcnow()
    req.Coin = coin
    req.Cfms = 0
    req.UserId = uid
    req.Amount = Decimal128(amount)
    req.Reference = ''

    col = get_col_fiatpaymentrequest()
    _id = col.insert_one(req.__dict__).inserted_id
    return map_fiatpaymentrequest(col.find_one(get_fiatreq_id_clause(_id)))


def add_opnotification(uid, opid):

    on = OpNotification()
    on.IsDeleted = 0
    on.CreatedAt = datetime.datetime.utcnow()
    on.OpId = str(opid)
    on.UserId = str(uid)

    col = get_col_opnotification()
    _id = col.insert_one(on.__dict__).inserted_id

    return map_opnotification(col.find_one(get_opnotification_id_clause(_id)))


def remove_opnotification(id):

    col = get_col_opnotification()
    col.update_one({'_id': id}, {'$set': {'IsDeleted': 1}})


def get_opnotifications(uid):

    col = get_col_opnotification()
    clause = get_opnotification_uid_clause(uid)
    cur = col.find(clause)
    return map_many_opnotifications(cur)


def get_ethtxtopup_to_watch():

    col = get_col_ethtopuprequest()
    cur = col.find({'Cfms': 0})
    res = []
    for t in cur:
        tmapped = map_EthTopUpRequest(t)
        res.append(tmapped)
    return res


def confirm_ethtopuprequest(hash):

    col = get_col_ethtopuprequest()
    col.update_one({'Hash': hash}, {'$set': {'Cfms': 1}})


def confirm_fiatpaymentrequest(id):

    col = get_col_fiatpaymentrequest()
    col.update_one({'_id': id}, {'$set': {'Cfms': 1}})


def get_operations(uid):

    wallets = get_wallets_by_uid(uid)
    if (len(wallets) == 0):
        return []

    col = get_col_ops()
    clause = get_operation_wallets_clause(wallets)

    cur = col.find(clause).sort([("CreatedAt", -1)]).limit(30)
    res = []
    for o in cur:
        res.append(map_operation(o))

    return res


def get_uids():
    col = get_col_wallets()
    return col.find({}).distinct("UserId")


def get_top_ups(coin, collected=False):

    assert not collected, "Collected top-ups are not allowed to fetch"

    if not collected:
        col = get_col_ops()
        cur = col.find(get_topup_not_collected_clause(coin))
        return map_many_operations(cur)


def cold_op(id):

    col = get_col_ops()
    col.update_one({'_id': id}, {'$set': {'InColdWallet': 1}})


def get_fiatpaymentrequest_by_ref(ref):
    col = get_col_fiatpaymentrequest()
    return map_fiatpaymentrequest(
                col.find_one(get_fiatreq_id_clause(ObjectId(ref))))


def eth_post_parsed_block(idx, tag):

    parsed_block = {
        'idx': idx,
        'tag': tag,
        'd': datetime.datetime.utcnow()
    }

    db['eth_parsed_blocks'].insert_one(parsed_block)


def eth_get_last_parsed_block(default):
    rec = list(db['eth_parsed_blocks'].find({})
                                      .sort('idx', pymongo.DESCENDING)
                                      .limit(1))
    if (len(rec) == 0):
        return default
    return int(rec[0]['idx']) + 1
