from __future__ import absolute_import
from celery.schedules import crontab

# Celery application definition
# http://docs.celeryproject.org/en/v4.0.2/userguide/configuration.html
SECRET_KEY = 'asd'
CELERY_IMPORTS = ("tasks")
CELERY_BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TASK_SERIALIZER = 'json'
CELERY_BEAT_SCHEDULE = {
    'test-task': {
        'task': 'tasks.getrates_task',
        'schedule': crontab(minute='*/1')
    }
}
