import blockcypher as bc
import wallet.config as cfg
import wallet.storage as db
import requests
from web3 import Web3
from dotenv import load_dotenv
import os
from decimal import Decimal
import auth.token as auth
import logging
import messages as global_mes

logging.basicConfig(
                    format=global_mes.LogFormat,
                    level=logging.INFO)

logger = logging.getLogger(__name__)

load_dotenv()


class BtcOps:

    def _replace_test_coin(self, coin):
        if cfg.BC_TEST:
            return cfg.BC_TEST_COIN
        else:
            return coin

    def __init__(self, coin, uid):
        assert uid is not None, "uid is not defined"
        assert coin is not None, "coin is not defined"

        self.coin = coin
        self.uid = uid

    def __get_bc_info(self, addr):
        return bc.get_address_overview(addr,
                                       self._replace_test_coin(self.coin),
                                       cfg.BC_TOKEN)

    def get_wallet(self):
        wallet = db.get_wallet_by_uid(self.uid, self.coin)
        if (wallet is None):
            addr = self.__gen_addr()
            hook_id = self._top_up_hook(addr["address"])
            wallet = db.new_wallet(self.uid, self.coin,
                                   addr["address"],
                                   addr["private"],
                                   hook_id)

        wallet.BCinfo = self.__get_bc_info(wallet.Address)

        return wallet

    def __gen_addr(self):
        return bc.generate_new_address(
            self._replace_test_coin(self.coin), cfg.BC_TOKEN)

    def _top_up_hook(self, address):
        return bc.subscribe_to_address_webhook(
                                callback_url=cfg.BC_HOOK_URL,
                                subscription_address=address,
                                event='confirmed-tx',
                                coin_symbol=self._replace_test_coin(self.coin),
                                api_key=cfg.BC_TOKEN)

    def to_cold_address(self, from_addr, cold_addr, pk, amount):
        tx = bc.simple_spend(from_privkey=pk,
                             to_address=cold_addr,
                             to_satoshis=int(amount),
                             coin_symbol=self._replace_test_coin(self.coin),
                             api_key=cfg.BC_TOKEN)
        return tx

    def from_cold_address(self, to_addr, amount):
        cold_addr = bc.get_address_overview(os.getenv("COLD_BTC_ADDR"),
                                            self._replace_test_coin(self.coin),
                                            cfg.BC_TOKEN)
        balance = int(cold_addr["balance"])
        allowed_balance = (cfg.COLD_WALLET_WITHDRAW_LIMIT/100)*balance

        assert amount < allowed_balance and amount > 0, \
            "Withdrawal is not allowed due to the limit %r" % amount

        tx = bc.simple_spend(from_privkey=os.getenv("COLD_BTC_PK"),
                             to_address=to_addr,
                             to_satoshis=int(amount),
                             coin_symbol=self._replace_test_coin(self.coin),
                             api_key=cfg.BC_TOKEN)

        return tx


class EthOps:

    def __init__(self, coin, uid):
        assert uid is not None, "uid is not defined"
        assert coin is not None, "coin is not defined"

        w3 = Web3(Web3.HTTPProvider(cfg.ETH_HTTP_PROVIDER))

        self.w3 = w3
        self.coin = coin
        self.uid = uid

    def __get_bc_info(self, addr):
        return bc.get_address_overview(addr, self.coin, cfg.BC_TOKEN)

    def get_wallet(self, isERC20=False):

        wallet = db.get_wallet_by_uid(self.uid, self.coin)

        if (wallet is None):

            addr = None
            pk = None

            if isERC20:
                w_eth = db.get_wallet_by_uid(self.uid, cfg.ETH_COIN)
                addr = w_eth.Address
                pk = w_eth.PrivateKey

            if addr is None and pk is None:
                a = self.__gen_acc()
                addr = a.address
                pk = a.privateKey.hex()

            addr_checksum = Web3.toChecksumAddress(addr)
            wallet = db.new_wallet(self.uid, self.coin,
                                   addr_checksum, pk, '')

        db.add_EthWatchAddress(wallet.Address)
        return wallet

    def __gen_acc(self):
        return self.w3.eth.account.create('SOME MAGIC PHRASE')

    def to_cold_address(self, from_addr, cold_addr, pk, amount):

        tr = {
            'from':  self.w3.toChecksumAddress(from_addr),
            'value': int(amount),
            'gas': 200000,
            'gasPrice': self.w3.eth.gasPrice*3,
            'nonce': self.w3.eth.getTransactionCount(from_addr),
            'to':  self.w3.toChecksumAddress(cold_addr),
            'data': b''
        }

        print(tr)

        signed_txn = self.w3.eth.account.signTransaction(tr, pk)
        tx = self.w3.eth.sendRawTransaction(signed_txn.rawTransaction)
        self.w3.eth.waitForTransactionReceipt(tx)
        return tx

    def to_cold_address_erc20(self, token, from_addr, cold_addr, pk, amount):

        cfg_token = cfg.ETH_ERC20[token]
        assert cfg_token is not None, "Token is not in configuration %r" \
            % token

        erc20 = self.w3.eth.contract(
            address=self.w3.toChecksumAddress(cfg.ETH_ERC20[token]['address']),
            abi=cfg.ETH_ERC20[token]['abi'])

        erc20_txn = erc20.functions.transfer(
            self.w3.toChecksumAddress(cold_addr),
            int(amount)).buildTransaction(
                {
                    'from': self.w3.toChecksumAddress(from_addr),
                    'gas': 200000,
                    'gasPrice': self.w3.eth.gasPrice*3,
                    'nonce': self.w3.eth.getTransactionCount(from_addr)
                }
            )

        signed_txn = self.w3.eth.account.signTransaction(erc20_txn, pk)
        tx = self.w3.eth.sendRawTransaction(signed_txn.rawTransaction)
        self.w3.eth.waitForTransactionReceipt(tx)
        return tx

    def from_cold_address(self, to_addr, amount):

        cold_addr = self.w3.toChecksumAddress(os.getenv("COLD_ETH_ADDR"))
        pk = os.getenv("COLD_ETH_PK")
        balance = self.w3.eth.getBalance(cold_addr)
        allowed_balance = (cfg.COLD_WALLET_WITHDRAW_LIMIT/100)*balance

        assert amount < allowed_balance and amount > 0, \
            "Withdrawal is not allowed due to the limit %r" % amount

        tr = {
                'from':  self.w3.toChecksumAddress(cold_addr),
                'value': int(amount),
                'gas': 200000,
                'gasPrice': self.w3.eth.gasPrice*3,
                'nonce': self.w3.eth.getTransactionCount(cold_addr),
                'to':  self.w3.toChecksumAddress(to_addr),
                'data': b''
        }

        print(tr)

        signed_txn = self.w3.eth.account.signTransaction(tr, pk)
        tx = self.w3.eth.sendRawTransaction(signed_txn.rawTransaction)
        # self.w3.eth.waitForTransactionReceipt(tx)
        return tx.hex()

    def from_cold_erc20_address(self, token, to_addr, amount):

        cold_addr = self.w3.toChecksumAddress(os.getenv("COLD_ETH_ADDR"))
        pk = os.getenv("COLD_ETH_PK")

        erc20 = self.w3.eth.contract(
            address=self.w3.toChecksumAddress(cfg.ETH_ERC20[token]['address']),
            abi=cfg.ETH_ERC20[token]['abi'])

        balance = erc20.call().balanceOf(cold_addr)

        allowed_balance = (cfg.COLD_WALLET_WITHDRAW_LIMIT/100)*balance

        print("Amount:{0}, Allowed {1}".format(amount, allowed_balance))

        assert amount < allowed_balance and amount > 0, \
            "Withdrawal is not allowed due to the limit %r" % amount

        erc20_txn = erc20.functions.transfer(
            self.w3.toChecksumAddress(to_addr),
            int(amount)).buildTransaction(
            {
                'from': self.w3.toChecksumAddress(cold_addr),
                'gas': 200000,
                'gasPrice': self.w3.eth.gasPrice*3,
                'nonce': self.w3.eth.getTransactionCount(cold_addr)
            }
            )

        signed_txn = self.w3.eth.account.signTransaction(erc20_txn, pk)
        tx = self.w3.eth.sendRawTransaction(signed_txn.rawTransaction)
        return tx.hex()


class ExchangeOps:

    def __init__(self, symbol, volume, price, type, uid):

        assert symbol is not None, "Symbol can't be undefined"
        assert volume > 0, "Volume should be more than zero"
        assert price > 0, "Price should be more than zero"
        assert type == 0 or type == 1, "Type should be 0 or 1"
        assert uid is not None, "UserId can't be undefined"

        self.symbol = symbol
        self.volume = volume
        self.price = price
        self.type = type
        self.uid = uid

    def post_trade(self):

        headers = {'Authorization': "Bearer %s" % auth.get_token()}
        print(headers)
        url = "{0}{1}/add".format(cfg.EXCHANGE_PROVIDER, self.symbol)
        r = requests.post(url, json={
                  "id": 0,
                  "rootOrderId": 0,
                  "parentOrderId": 0,
                  "orderType": self.type,
                  "orderState": 0,
                  "price": self.price,
                  "volume": self.volume,
                  "userId": self.uid
        }, headers=headers)

        print(r.__dict__)
        return r.json()


class OpOps:

    def __init__(self, uid):

        assert uid is not None, "UserId can't be None"
        self.uid = uid

    def get_ops(self):
        return db.get_operations(self.uid)


class WalletOps:

    __CURR_DEC_MAPPING = {'btc': 8, 'eth': 18, 'gvc': 8, 'cny': 2, 'ngn': 2}

    def _precise_amount(self, amount):
        return Decimal(str(amount))

    def process_amount_from_normal_dec(self, amount):

        assert isinstance(amount, Decimal), "Amount should be a decimal type"

        decs = self.__CURR_DEC_MAPPING[self.wallet.Coin.lower()]
        assert decs is not None, "Dec mapping for the coin is not defined"
        return amount*(10**decs)

    def __process_amount_to_normal_dec(self, amount):

        assert isinstance(amount, Decimal), "Amount should be a decimal type"

        decs = self.__CURR_DEC_MAPPING[self.wallet.Coin.lower()]
        assert decs is not None, "Dec mapping for the coin is not defined"
        return amount/(10**decs)


# TODO do constructor refactoring in __init_with_addr, __init with op_id manner
    def __init__(self, coin=None, uid=None, addr=None, op_id=None, clone=False,
                 clone_coin=None):

        w = None

        if (op_id is not None):

            w = db.get_wallet_by_opid(op_id)

        elif (addr is not None):

            assert coin is not None, "coin is not defined"

            w = db.get_wallet_by_address(coin, addr)

            if (w is None and clone):
                assert clone_coin is not None, "clone_coin is not defined"
                w = db.clone_wallet_by_address(coin, clone_coin, addr)

        else:
            assert uid is not None, "uid is not defined"
            assert coin is not None, "coin is not defined"

            w = db.get_wallet_by_uid(uid, coin)

        assert w is not None, "Wallet not found"

        self.wallet = w

    def _roll_hold(self, amount, reference, target_wallet=None):

        assert self.wallet.Balance >= amount, \
                "Amount: %f should be less than balance: %f" \
                % (amount, self.wallet.Balance)

        self.wallet.Balance -= amount
        self.wallet.Hold += amount

        return db.add_hold_op(self.wallet, amount, reference)

    def _roll_transfer(self, amount, reference, target_wallet=None):

        assert target_wallet is not None, "target_wallet is not defined"
        assert target_wallet.Coin == self.wallet.Coin, \
            "Wallets should have same coin type"
        assert reference is not None, "reference is not defined"
        assert self.wallet.Hold >= amount, \
            "Amount should be less than Hold: %r " % amount
        assert str(target_wallet.Id) != str(self.wallet.Id), \
            "You can't do transfer to the same wallet"

        db.check_hold_balance(reference, amount)

        target_wallet.Balance += amount
        self.wallet.Hold -= amount

        op_id = db.add_transfer_op(self.wallet, target_wallet, amount,
                                   reference)

        db.update_wallet_balance(target_wallet)
        return op_id

    def _roll_writeoff(self, amount, reference, target_wallet=None):

        assert self.wallet.Hold >= amount, \
            "Amount should be less than Hold"

        db.check_hold_balance(reference, amount)

        self.wallet.Hold -= amount
        return db.add_writeoff_op(self.wallet, amount, reference)

    def _roll_release(self, amount, reference, target_wallet=None):

        assert reference is not None, \
            "Reference can't be None for Release operations"
        assert self.wallet.Hold >= amount, \
            "Amount of %s should be less than Hold: %s" \
            % (amount, reference)

        db.check_hold_balance(reference, amount)

        self.wallet.Hold -= amount
        self.wallet.Balance += amount

        return db.add_release_op(self.wallet, amount, reference)

    def _roll_withdraw(self, amount, reference, target_wallet=None):

        assert self.wallet.Hold >= amount, \
            "Amount of %s should be less than Hold: %s" \
            % (amount, reference)

        self.wallet.Hold -= amount
        return db.add_withdraw_op(self.wallet, amount, reference)

    def _roll_topup(self, amount, reference, target_wallet=None):

        self.wallet.Balance += amount
        return db.add_topup_op(self.wallet, amount, reference)

    def _roll_commission(self, amount, reference, target_wallet=None):
        raise ValueError("Operation type is not supported")

    def _roll_cold(self, amount, reference, target_wallet=None):
        db.cold_op(reference)
        return None

    def create_wallets(uid):

        # getting BTC wallet ready
        btc_ops = BtcOps(cfg.BC_COIN, str(uid))
        bw = btc_ops.get_wallet()

        # getting ETH wallet ready
        eth_ops = EthOps(cfg.ETH_COIN, str(uid))
        ew = eth_ops.get_wallet()

        # getting GVC wallet ready
        gvc_ops = EthOps(cfg.ETH_ERC20['gvc']['symbol'], str(uid))
        gw = gvc_ops.get_wallet(isERC20=True)

        return [bw, ew, gw]

    def roll(self, op_type, amount, target_wallet=None, reference=None,
             to_norm_dec=True, callback=None):

        assert op_type is not None, "op_type is not defined"
        assert amount > 0, "amount should be more than zero"

        if (not isinstance(amount, Decimal)):
            amount = self._precise_amount(amount)

        amount = self.__process_amount_to_normal_dec(amount) \
            if (to_norm_dec) else amount

        op_id = ''

        roll_mappings = {
            db.OpTypes.Hold: self._roll_hold,
            db.OpTypes.Transfer: self._roll_transfer,
            db.OpTypes.WriteOff: self._roll_writeoff,
            db.OpTypes.TopUp: self._roll_topup,
            db.OpTypes.Cold: self._roll_cold,
            db.OpTypes.Withdraw: self._roll_withdraw,
            db.OpTypes.Commission: self._roll_commission,
            db.OpTypes.Release: self._roll_release
        }

        roll_func = roll_mappings.get(op_type, None)

        if (roll_func):
            op_id = roll_func(amount, reference, target_wallet)
            db.update_wallet_balance(self.wallet)
            if (callback is not None):
                    callback(op_id, target_wallet)
            return op_id
        else:
            raise ValueError("Operation type is not supported: %r" % op_type)
