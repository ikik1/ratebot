from __future__ import absolute_import, unicode_literals
from celery import task
import cmcapi as cmcapi
import storage as storage


@task()
def getrates_task():
    print("Getting rates")
    print(storage.save_ticks(cmcapi.get_ticks()))
