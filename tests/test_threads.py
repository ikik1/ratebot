from threading import Thread
import logging
import time

logging.basicConfig(level=logging.INFO,
                    format='[%(asctime)s] [%(levelname)s] (%(threadName)-10s) \
                            %(message)s',)


def log_loop():

    while True:
        logging.info('Tick from th 1')
        time.sleep(2)


def cfms_loop():

    while True:
        logging.info('Tick from th 2')
        time.sleep(2)


def main():

    logging.info('Getting ready to strart threads ...')

    w_log = Thread(name="Thread 1", target=log_loop,
                   args=(), daemon=False)
    w_log.start()

    w_cfms = Thread(name="Thread 2", target=cfms_loop,
                    args=(), daemon=False)
    w_cfms.start()


if __name__ == '__main__':
    main()
