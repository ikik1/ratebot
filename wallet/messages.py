_Wallet_TEST = "Echo wallet {0}"


_Wallet_Summary = """
--== <b>{3}</b> wallet summary ==--
Address:
{0}
Balance: <b>{1}</b>
Hold balance: {2}
"""


_Ops_Summary = """
Type: <b>{0}</b> {4}
Date: {1}
Amount: <b>{2}</b>
Id: {3}
"""

_Ops_Detail = """
You have new operation on the wallet:
Type: <b>{0}</b> {4}
Date: {1}
Amount: <b>{2}</b>
Id: {3}
"""


_ExchangeLog = """
--== Exchange log {1} ==--
{0}
{2}
"""

_TopUpCurrency = """
Please choose a coin:
"""

_TradeLog = "{0} {1}\n"


_TopUpAddress = """
Below is your address for top up, copy it and and paste to your <b>{0}</b>
wallet app to send coins
"""


_TopUpAddressInfo = "<b>{0}</b>"

_BuyCoin = "Please choose a coin to buy:"

_SellCoin = "Please choose a coin to sell:"

_BuyCoinAmount = """Please enter the amount of {0}
 you want to buy (example: 1.23)"""

_WithdrawCoinAmount = """Please enter the amount of {0}
you want to withdraw (example: 1.23)"""

_TopUpFiatAmount = """Please enter the amount of {0}
you want to top up (example: 1.23)"""

_SellCoinAmount = """Please enter the amount of {0}
you want to sell (example: 1.23)"""

_SecondCoin = "Please choose a coin you want to {0} <b> {1}</b> {2} for:"

_ConfirmBuyTrade = """You are about to {5} <b>{0:f} {1}</b>
at best price of {2} {3} per 1 {1}.
Total amount of <b>{4:f} {3}</b> will be deducted from your balance:
<b>{6:f} {3}</b>"""

_ConfirmSellTrade = """You are about to {5} <b>{0:f} {1}</b>
at best price of {2} {3} per 1 {1}.
Total amount of <b>{0:f} {1}</b> will be deducted from your balance:
<b>{6:f} {1}</b>"""

_CancelTrade = "Your operation has been canceled"

_WithdrawAddress = """Please paste a valid {0} address
you want to withdraw your funds to:"""

_WithdrawConfirm = """Please confirm that you want to withdraw
<b>{0:f} {1}</b> to <b>{2}</b> address"""

_WithdrawalSuccess = """Please refer to your tx number on {2} explorer:
<b>{0}</b>
Amount: <b>{1} {2}</b>
Address: <b>{3}</b>"""

_AliPayTopUp = """To top up your wallet,
please scan this QR code with your AliPay app:"""

_AliPayProgress = """Your AliPay QR is cooking, please wait ..."""


_FiatWithdrawalInfo = """Please provide us with your card information
to do withdrawals:"""


def get_wallet_summary(wallets):
    mes = ''
    for w in wallets:
        mes += _Wallet_Summary.format(w.Address,
                                      '{0:f}'.format(w.Balance),
                                      '{0:f}'.format(w.Hold),
                                      w.Coin.upper())
    return mes


def get_op_direction(op, uid):
    if (op.Type == 2 and op.UserId == uid):
        return "OUT"
    elif (op.CreditAmount > 0):
        return "IN"
    else:
        return "OUT"


def format_op_type(t):
    if t == 1:
        return "Hold"
    elif t == 2:
        return "Transfer"
    elif t == 3:
        return "WriteOff"
    elif t == 4:
        return "TopUp"
    elif t == 5:
        return "Commission"
    elif t == 6:
        return "Release"
    elif t == 8:
        return "Withdrawal"


def format_amount(o):
    amount = o.CreditAmount
    if (o.CreditAmount == 0):
        amount = o.DebitAmount

    return "{0} {1}".format(amount, o.Coin.upper())


def format_id(id):
    return "..." + id[-3:]


def get_ops_summary(ops, uid):
    mes = ''
    for o in ops:
        mes += _Ops_Summary.format(
                    format_op_type(o.Type),
                    o.CreatedAt,
                    format_amount(o),
                    format_id(str(o.Id)),
                    get_op_direction(o, uid))

    return mes


def get_notification(o, uid):
        mes = ''

        mes = _Ops_Detail.format(
                    format_op_type(o.Type),
                    o.CreatedAt,
                    format_amount(o),
                    format_id(str(o.Id)),
                    get_op_direction(o, uid))

        return mes


def get_trade_optype(t):

    if (t["OperationType"] == 0):
        return "Created"
    elif (t["OperationType"] == 1):
        return "Added"
    elif (t["OperationType"] == 5):
        return "Canceled"
    elif (t["OperationType"] == 2):
        return "Traded"
    elif (t["OperationType"] == 3):
        return "Closed"
    elif (t["OperationType"] == 4):
        return "Partialy closed"
    else:
        return "Unknown"


def get_trade_summary(res, symbol):

    t_log = ''
    trs = res["TradeOperations"]
    closed = False
    for t in trs:
        closed = (t["OperationType"] == 3)
        t_log += _TradeLog.format(get_trade_optype(t), t["Message"])

    s_processing = "Please wait for your order to be processed ..."
    if (closed):
        s_processing = "Trade closed."
    return _ExchangeLog.format(t_log, symbol, s_processing)


def get_deposit_currency():
    return _TopUpCurrency


def get_wallet_address(coin):
    return _TopUpAddress.format(coin.upper())


def get_wallet_address_info(address):
    return _TopUpAddressInfo.format(address)


def get_buy_coin():
    return _BuyCoin


def get_buy_coin_amount(coin):
    return _BuyCoinAmount.format(coin.upper())


def get_sell_coin_amount(coin):
    return _SellCoinAmount.format(coin.upper())


def get_withdraw_coin_amount(coin):
    return _WithdrawCoinAmount.format(coin.upper())


def get_topup_fiat_amount(coin):
    return _TopUpFiatAmount.format(coin.upper())


def get_withdraw_address(cmd):
    parts = cmd.split('_')
    return _WithdrawAddress.format(parts[1].upper())


def get_sell_coin():
    return _SellCoin


def get_second_coin(cmd):
    parts = cmd.split('_')
    return _SecondCoin.format(parts[0], parts[1].upper(), parts[2])


def get_confirmation_buy_trade(amount, first_coin, rate, second_coin,
                               total_to_spend, cmd, balance):
    return _ConfirmBuyTrade.format(amount, first_coin.upper(),
                                   rate, second_coin.upper(),
                                   total_to_spend, cmd, balance)


def get_confirmation_sell_trade(amount, first_coin, rate, second_coin,
                                total_to_spend, cmd, balance):
    return _ConfirmSellTrade.format(amount, first_coin.upper(),
                                    rate, second_coin.upper(),
                                    total_to_spend, cmd, balance)


def get_confirmation_withdraw(amount, coin, address):
    return _WithdrawConfirm.format(amount, coin.upper(), address)


def get_cancel_trade():
    return _CancelTrade


def get_confirm_withdrawal(tx, amount, coin, address):
    print(_WithdrawalSuccess.format(tx, amount, coin, address))
    return _WithdrawalSuccess.format(tx, amount, coin, address)


def get_alipay_progress():
    return _AliPayProgress


def get_alipay_topup():
    return _AliPayTopUp


def get_withdrawal_fiat_info():
    return _FiatWithdrawalInfo
