import wallet.operations as ops
import wallet.storage as db
import wallet.config as cfg
from decimal import Decimal


class WithdrawalManager:

    def __init__(self, coin, uid):

        self._w_op = ops.WalletOps(coin=coin, uid=uid)

    def _parse_reference(self, ref):

        parts = ref.reference.split(':')
        assert len(parts) == 2, "Reference for withdrawal is invalid %r" \
            % ref

        return {'addr': parts[1], 'amount': float(parts[0])}

    def _process_withdrawal(self, op):

        parsed_ref = self._parse_reference(op)

        amount = self._w_op.process_amount_from_normal_dec(
                                Decimal(parsed_ref['amount']))

        if (op.Coin == cfg.BC_COIN):
            btc_ops = ops.BtcOps(op.Coin, op.UserId)
            return btc_ops.from_cold_address(parsed_ref['addr'],
                                             amount)
        elif (op.Coin == cfg.ETH_COIN):
            eth_ops = ops.EthOps(op.Coin, op.UserId)
            return eth_ops.from_cold_address(parsed_ref['addr'], amount)
        else:
            eth_ops = ops.EthOps(op.Coin, op.UserId)
            return eth_ops.from_cold_erc20_address(op.Coin,
                                                   parsed_ref['addr'], amount)

    def request_withdraw(self, amount, address):

        hop = self._w_op.roll(db.OpTypes.Hold, amount,
                              to_norm_dec=False,
                              reference="{0}:{1}".format(amount, address))
        return hop

    def __notify(self, op, target_wallet=None):
            db.add_opnotification(op.UserId, op.Id)

    def confirm_withdraw(self, op, amount):
        op = db.get_op_by_id(op.Id)
        try:
            db.check_hold_balance(str(op.Id), amount)

            tx = self._process_withdrawal(op)
            w_op = self._w_op.roll(db.OpTypes.Withdraw, amount,
                                   reference=op.Id,
                                   callback=self.__notify,
                                   to_norm_dec=False)
            return {'op_id': str(w_op.Id), 'tx': tx}
        except AssertionError as e:
            self._w_op.roll(db.OpTypes.Release, amount,
                            reference=str(op.Id), to_norm_dec=False)
            raise e

    def cancel_withdraw(self, amount, op_id):
        return self._w_op.roll(db.OpTypes.Release, amount, op_id,
                               to_norm_dec=False)
