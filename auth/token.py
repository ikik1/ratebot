from dotenv import load_dotenv
import os
import requests
import requests_cache

requests_cache.install_cache('token_cache', backend='memory', expire_after=10,
                             include_get_headers=True)

load_dotenv()


def get_token():

    url = "%sconnect/token" % os.getenv('AUTH_PROVIDER')

    headers = {
        'Authorization': "Basic %s" % os.getenv('AUTH_BASE64'),
        'Cache-Control': "no-cache",
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    response = requests.request("POST", url, headers=headers, verify=False,
                                data={'grant_type': 'client_credentials', 'scope': 'obapi'})

    return response.json()['access_token']
