import requests

url = "https://auth.stg.x-2.io/connect/token"

headers = {
    'Authorization': "Basic VG9rZW5Ob3RpZmllckJvdDpUb2tlbk5vdGlmaWVyQm90U2VjcmV0",
    'Cache-Control': "no-cache",
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded'
}

response = requests.request("POST", url, headers=headers, verify=False,
                            data={'grant_type': 'client_credentials', 'scope': 'obapi'})


token = response.json()['access_token']

url = "https://obapi.stg.x-2.io/symbols"

headers = {
    'Authorization': "Bearer %s" % token,
    'Cache-Control': "no-cache",
    'Accept': 'application/json',
}

response = requests.request("GET", url, headers=headers, verify=False)

print(response.text)
