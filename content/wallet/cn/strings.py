_IN_OUT = ["传入", '外向']

_Types = {1: "持有", 2: "转移", 3: "注销", 4: "存款", 5: "费",
          6: "释放", 8: "退出"}

_TradeTypes = {0: "Created", 1: "Added", 5: "Canceled", 2: "Traded",
               3: "Closed", 4: "Partialy closed", -1: "Unknown"}

_SELL_BUY = {'sell': '卖', 'buy': '买'}

_Wallet_TEST = "Echo wallet {0}"

_Wallet_Summary = """
--== <b>{3}</b> 我的钱包 ==--
地址:
{0}
余额: <b>{1}</b>
预付款: {2}
"""

_Ops_Summary = """
类型: <b>{0}</b> {4}
日期: {1}
量: <b>{2}</b>
Id: {3}
"""

_Ops_Detail = """
账单详情:
当前状态: <b>{0}</b> {4}
日期: {1}
金额: <b>{2}</b>
单号: {3}
"""

_ExchangeLog = """
--== 交易记录 {1} ==--
{0}
{2}
"""

_TopUpCurrency = """
请选择币种:
"""

_TradeLog = "{0} {1}\n"

_TopUpAddress = """
以下是你充值的地址, 复制并粘贴到你的 <b>{0}</b>
钱包应用程序发送到你充值帐户
"""

_TopUpAddressInfo = "<b>{0}</b>"

_BuyCoin = """请选择您想购买的币种:"""

_SellCoin = """请选择您想卖出的币种:"""

_BuyCoinAmount = """请输入您要购买的 {0}
金额 (例如: 1.23)"""

_WithdrawCoinAmount = """请输入您要提取的 {0}
金额 (例如: 1.23)"""

_TopUpFiatAmount = """请输入您要充值的 {0}
金额 (例如: 1.23)"""

_SellCoinAmount = """请输入您要卖出的 {0}
金额 (example: 1.23)"""

_SecondCoin = "请选择您要使用的币种 {0} <b> {1}</b> {2}"

_ConfirmBuyTrade = """您即将 {5} <b>{0:f} {1}</b>
的最佳价格 {2} {3} 是 1 {1}.
总金额 <b>{4:f} {3}</b> 扣除将从您的总余额:
<b>{6:f} {3}</b>"""

_ConfirmSellTrade = """您即将 {5} <b>{0:f} {1}</b>
的最佳价格 {2} {3} 是 1 {1}.
总金额 <b>{0:f} {1}</b> 扣除将从您的总余额:
<b>{6:f} {1}</b>"""

_CancelTrade = "您的操作已被取消"

_WithdrawAddress = """请粘贴您想要 {0} 地址提取资金到:"""

_WithdrawConfirm = """请确认您要提取
<b>{0:f} {1}</b> 到 <b>{2}</b> 地址"""

_WithdrawalSuccess = """请查看您的交易单号在 {2} 浏览器:
<b>{0}</b>
金额: <b>{1} {2}</b>
地址: <b>{3}</b>"""

_AliPayTopUp = """为了充值你的钱包,
请用你的支付宝扫描这个二维码
(二维码过了1分钟到期, 单笔 最低 10 最高 5000):"""

_AliPayProgress = """您的支付宝二维码正在生成中, 请稍候 ..."""


_FiatWithdrawalInfo = """请向我们提供您的银行卡信息以进行提款
(最低能提是100人民币):"""

_SProc = "请等待您的订单被处理 ..."
_SProcClosed = "交易结束"
