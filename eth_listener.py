from web3 import Web3, WebsocketProvider
import time
import logging
from threading import Thread
from queue import Queue
import wallet.config as cfg
import wallet.storage as db
from wallet.operations import WalletOps
from eth_abi import decode_abi
from eth_utils import (
     encode_hex,
     function_signature_to_4byte_selector,
     to_bytes
)
from decimal import Decimal
import sys


ERC20_TRANSFER_ABI_PREFIX = encode_hex(
    function_signature_to_4byte_selector('transfer(address, uint256)'))


logging.basicConfig(level=logging.INFO,
                    format='[%(asctime)s] [%(levelname)s] (%(threadName)-10s) \
                            %(message)s',)

wp = WebsocketProvider(cfg.ETH_PROVIDER, websocket_kwargs={'timeout': 5})
w3 = Web3(wp)

w3http = Web3(Web3.HTTPProvider(cfg.ETH_HTTP_PROVIDER))

if (len(sys.argv) == 3 and sys.argv[2] == 'poa'):
    from web3.middleware import geth_poa_middleware
    w3.middleware_stack.inject(geth_poa_middleware, layer=0)
    w3http.middleware_stack.inject(geth_poa_middleware, layer=0)

CUR_BLOCK_INDEX = w3http.eth.blockNumber


class UniqueQueue(Queue):

    def _init(self, maxsize):
        self.all_items = set()
        Queue._init(self, maxsize)

    def put(self, item, block=True, timeout=None):
        if item not in self.all_items:
            self.all_items.add(item)
            Queue.put(self, item, block, timeout)


def __watch_tx(tx, wl, coin, amount=0, to=None):

    val = (tx.value, amount)[amount > 0]
    to_addr = (tx.to, to)[to is not None]

    txo = db.add_EthTopUpRequest(tx.hash.hex(), coin,
                                 w3.toChecksumAddress(to_addr),
                                 val)

    wl.put(txo)

    logging.info('Appended to a watch list')
    logging.info(txo)
    logging.info(wl)


def checkERC20(tx, to_adddrs):
    logging.info(tx)
    for key in cfg.ETH_ERC20:
        if (tx and tx.to
            and cfg.ETH_ERC20[key]['address'].lower() == tx.to.lower()
            and tx.input
            and tx.input.lower().startswith(
                ERC20_TRANSFER_ABI_PREFIX.lower())):
                tx_input = tx.get('input')
                inp_part = to_bytes(
                    hexstr=tx_input[len(ERC20_TRANSFER_ABI_PREFIX):])
                logging.info(inp_part)
                to, amount = decode_abi(['address', 'uint256'], inp_part)
                logging.info("To: %s, Amount: %s", to, amount)
                if (w3.toChecksumAddress(to) in to_adddrs):
                    return {
                            'to': w3.toChecksumAddress(to),
                            'amount': amount,
                            'symbol': cfg.ETH_ERC20[key]['symbol']
                           }


def handle_event(event, wl):

    # iterate through ending transaction array and get transaction address

    subscr_addrs = db.get_ethaddr_to_watch()
    # logging.info(subscr_addrs)

    for hash in event:
        tr = w3.eth.getTransaction(hash)

        erc20 = checkERC20(tr, subscr_addrs)

        if (erc20):
            logging.info('Found ERC20 %s' % tr.hash)
            __watch_tx(tr, wl, erc20['symbol'].lower(),
                       amount=erc20['amount'],
                       to=erc20['to'])

        elif (tr is not None and tr.to is not None
              and tr.hash is not None
              and tr.value is not None
              and float(tr.value) > 0):
                addr_to = w3.toChecksumAddress(tr.to)
                if (addr_to in subscr_addrs):
                    __watch_tx(tr, wl, 'eth')


def clear_unconfirmed(wl):
    unconfirmed = db.get_ethtxtopup_to_watch()
    for tx in unconfirmed:
        wl.put(tx)


def lookup_in_blocks(bidx):

    logging.info('Current block number %s' % bidx)

    res = []
    block = w3http.eth.getBlock(bidx)

    if (not block):
        return None

    res.extend(block['transactions'])

    logging.info('Searching in block %s' % bidx)
    logging.info(res)

    return res


def log_loop(poll_interval, wl, tag):

    logging.info('Clearing unconfirmed ... ')
    clear_unconfirmed(wl)

    logging.info('Starting log watching loop ... ')

    wait = False

    while True:
        try:

            if (not wait):
                bidx = db.eth_get_last_parsed_block(CUR_BLOCK_INDEX)
                db.eth_post_parsed_block(bidx, tag)

            txs = lookup_in_blocks(bidx)

            if (txs is not None):

                logging.info("Getting data from ETH network:")
                logging.info(txs)
                handle_event(txs, wl)
                wait = False
            else:
                wait = True

            time.sleep(poll_interval)
        except Exception as e:
            logging.exception(e)


def __notify(op, target_wallet=None):
        db.add_opnotification(op.UserId, op.Id)


def cfms_loop(poll_interval, wlQ):

    logging.info('Starting confirmation loop ...')

    while True:
        try:
            logging.info("Waiting for cfms from the queue ...")
            qtx = wlQ.get()
            if (qtx.Cfms == 0):
                r = w3http.eth.getTransactionReceipt(qtx.Hash)
                logging.info('Receipt:')
                logging.info(r)
                if (r is not None and 'status' in r and r['status'] == 1):

                    topupreq = db.get_ethxttopup(qtx.Hash)
                    if (topupreq and topupreq.Cfms == 0):
                        wo = WalletOps(coin=qtx.Coin, addr=qtx.To,
                                       clone=True, clone_coin=cfg.ETH_COIN)
                        logging.info(wo.wallet)
                        op_id = wo.roll(db.OpTypes.TopUp,
                                        Decimal(qtx.Value), reference=qtx.Hash,
                                        callback=__notify)
                        logging.info(op_id)
                        qtx.Cfms = 1
                        db.confirm_ethtopuprequest(qtx.Hash)
                else:
                    wlQ.put(qtx)

            wlQ.task_done()

            time.sleep(poll_interval)
        except Exception as e:
            logging.exception(e)


def main():

    assert sys.argv[1] is not None, "Please set the name for script"

    tag = sys.argv[1]

    q = UniqueQueue()

    logging.info('Getting ready to strart threads ...')

    w_log = Thread(name="LogEthTxsThread", target=log_loop,
                   args=(2, q, tag), daemon=False)
    w_log.start()

    w_cfms = Thread(name="LogEthTxsCfmsThread", target=cfms_loop,
                    args=(2, q), daemon=False)
    w_cfms.start()


if __name__ == '__main__':
    main()
