_Welcome = """
XSquared Trading bot - is a trading bot connected to the full liquidity in the X2 exchange platform providing you with the following services:

1) Digital assets exchange
2) Real-time data and information from the digital asset markets
3) Digital asset wallet deposit and withdrawal system.
4) CNY deposits and withdrawals

All of your funds are secured with multi-sig cold wallet which you can track by following transaction history on public blockchain explorers.

In case of financial losses, exchange problems, malicious activity please contact us on telegram support line @XSquared Support Service.

Please type /Start in the bot chat screen to see all available commands.

Thank you for joining the XSquared family, we hope you enjoy the simplicity and convenience now at your fingertips!

Sincerely,
The XSquared Team,
https://x-2.io/
"""

_Menu = {"wallet": "Wallet", "buy": "Buy", "sell": "Sell", "top-up": "Top-up",
         "withdraw": "Withdraw", "history": "History", "options": "Options",
         "cny": "CNY", "ngn": "NGN", "confirm": "Confirm", "cancel": "Cancel", "market": "Market"}

_Main = "Please choose an option:"

_Market = "Choose a market pair:"

_Market_Orders = """
--== <b>{0}</b> open orders ==--
{1}
"""

_Market_Order = """
Type: {0}
Volume: {1}
Price: {2}
"""

_Order_Types = {0: "SELL", 1: "BUY"}
