
from telegram import InlineKeyboardButton
import wallet.config as cfg

command = {
    "loc": {
        "root": "loc",
        "en": "loc_en",
        "cn": "loc_cn"
    },
    "wallet": {
        "root": "wallet",
        "topup": {
            "root": "topup",
            "btc": "topup_btc",
            "eth": "topup_eth",
            "gvc": "topup_gvc"
        },
        "ops": {"root": "ops"},
        "withdraw": {
            "root": "withdraw",
            "btc": "withdraw_btc",
            "eth": "withdraw_eth",
            "gvc": "withdraw_gvc"
        }
    },
    "market": {
        "root": "market"
    },
    "back": {
        "root": "back"
    },
    "buy": {
        "root": "buy"
    },
    "sell": {
        "root": "sell"
    },
    "second": {
        "root": "second"
    },
    "confirm": {
        "root": "confirm",
        "trade": "confirm_trade",
        "withdraw": "confirm_withdraw"
    },
    "cancel": {
        "root": "cancel",
        "trade": "cancel_trade",
        "withdraw": "cancel_withdraw"
    }
}


def get_lang_menu():

    button_list = [
        InlineKeyboardButton("English",
                             callback_data=command["loc"]["en"]),
        InlineKeyboardButton("中文", callback_data=command["loc"]["cn"])
    ]

    return button_list


def get_main_menu(loc):

    mi = loc.get_menu()

    button_list = [
        InlineKeyboardButton(mi['market'],
                             callback_data=command["market"]["root"]),
        InlineKeyboardButton(mi['wallet'],
                             callback_data=command["wallet"]["root"]),
        InlineKeyboardButton(mi['buy'], callback_data="buy"),
        InlineKeyboardButton(mi['sell'], callback_data="sell")
    ]

    return button_list


def get_wallet_menu(loc):

    mi = loc.get_menu()

    button_list = [
        InlineKeyboardButton(mi['top-up'], callback_data="topup"),
        InlineKeyboardButton(mi['withdraw'], callback_data="withdraw"),
        InlineKeyboardButton(mi['history'], callback_data="ops"),
        InlineKeyboardButton(mi['options'], callback_data="back_main")
    ]

    return button_list


def get_coins_menu(cmd, loc):

    mi = loc.get_menu()

    button_list = [
        InlineKeyboardButton("BTC", callback_data="{0}_btc".format(cmd)),
        InlineKeyboardButton("ETH", callback_data="{0}_eth".format(cmd)),
        InlineKeyboardButton("GVC", callback_data="{0}_gvc".format(cmd)),
        InlineKeyboardButton(mi['cny'], callback_data="{0}_cny".format(cmd)),
        InlineKeyboardButton(mi['ngn'], callback_data="{0}_ngn".format(cmd))
    ]

    return button_list


def get_fiat_withdrawal(uid, cmd, loc, linked_cards=None):

    mi = loc.get_menu()

    button_list = [
        InlineKeyboardButton(mi['withdraw'],
                             url="{0}withdraw/fiat/alipay/form?uid={1}&id={2}"
                             .format(cfg.WITHDRAWAL_FORM_PROVIDER, uid, cmd)),
    ]

    return button_list


def get_fiat_alipay_topup(uid, amount, loc, linked_cards=None):

    mi = loc.get_menu()

    button_list = [
        InlineKeyboardButton(mi['top-up'],
                             url="{0}?uid={1}&coin=cny&amount={2}"
                             .format(cfg.ONEPAY_TOPUP_URL, uid, amount)),
    ]

    return button_list


def get_confirmation_menu(cmd, loc):

    mi = loc.get_menu()

    button_list = [
        InlineKeyboardButton(mi['confirm'],
                             callback_data="confirm_{0}".format(cmd)),
        InlineKeyboardButton(mi['cancel'], callback_data="cancel_{0}"
                             .format(cmd))
    ]

    return button_list


def get_market_menu(cmd):

    button_list = [
        InlineKeyboardButton("BTC_CNY", callback_data="{0}_btc_cny".format(cmd)),
        InlineKeyboardButton("ETH_CNY", callback_data="{0}_eth_cny".format(cmd)),
        InlineKeyboardButton("GVC_CNY", callback_data="{0}_gvc_cny".format(cmd)),
        InlineKeyboardButton("BTC_ETH", callback_data="{0}_btc_eth".format(cmd))
    ]

    return button_list
