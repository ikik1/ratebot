_IN_OUT = ['IN', 'OUT']

_Types = {1: "Hold", 2: "Transfer", 3: "WriteOff", 4: "TopUp", 5: "Commission",
          6: "Release", 8: "Withdrawal"}

_TradeTypes = {0: "Created", 1: "Added", 5: "Canceled", 2: "Traded",
               3: "Closed", 4: "Partialy closed", -1: "Unknown"}

_Wallet_TEST = "Echo wallet {0}"


_SELL_BUY = {'sell': 'sell', 'buy': 'buy'}


_Wallet_Summary = """
--== <b>{3}</b> wallet summary ==--
Address:
{0}
Balance: <b>{1}</b>
Hold balance: {2}
"""


_Ops_Summary = """
Type: <b>{0}</b> {4}
Date: {1}
Amount: <b>{2}</b>
Id: {3}
"""

_Ops_Detail = """
You have new operation on the wallet:
Type: <b>{0}</b> {4}
Date: {1}
Amount: <b>{2}</b>
Id: {3}
"""


_ExchangeLog = """
--== Exchange log {1} ==--
{0}
{2}
"""

_TopUpCurrency = """
Please choose a coin:
"""

_TradeLog = "{0} {1}\n"


_TopUpAddress = """
Below is your address for top up, copy it and paste to your <b>{0}</b>
wallet app to send coins
"""


_TopUpAddressInfo = "<b>{0}</b>"

_BuyCoin = "Please choose a coin to buy:"

_SellCoin = "Please choose a coin to sell:"

_BuyCoinAmount = """Please enter the amount of {0}
 you want to buy (example: 1.23)"""

_WithdrawCoinAmount = """Please enter the amount of {0}
you want to withdraw (example: 1.23)"""

_TopUpFiatAmount = """Please enter the amount of {0}
you want to top up (example: 1.23)"""

_SellCoinAmount = """Please enter the amount of {0}
you want to sell (example: 1.23)"""

_SecondCoin = "Please choose a coin you want to {0} <b> {1}</b> {2} for:"

_ConfirmBuyTrade = """You are about to {5} <b>{0:f} {1}</b>
at best price of {2} {3} per 1 {1}.
Total amount of <b>{4:f} {3}</b> will be deducted from your balance:
<b>{6:f} {3}</b>"""

_ConfirmSellTrade = """You are about to {5} <b>{0:f} {1}</b>
at best price of {2} {3} per 1 {1}.
Total amount of <b>{0:f} {1}</b> will be deducted from your balance:
<b>{6:f} {1}</b>"""

_CancelTrade = "Your operation has been canceled"

_WithdrawAddress = """Please paste a valid {0} address
you want to withdraw your funds to:"""

_WithdrawConfirm = """Please confirm that you want to withdraw
<b>{0:f} {1}</b> to <b>{2}</b> address"""

_WithdrawalSuccess = """Please refer to your tx number on {2} explorer:
<b>{0}</b>
Amount: <b>{1} {2}</b>
Address: <b>{3}</b>"""

_AliPayTopUp = """To top-up your wallet please click “top-up” below.( You will be taken to the payment page via your web browser)
(QR code expires in 1 minute, min amount is 10 CNY max 5000 CNY):"""

_AliPayProgress = """Your AliPay QR is generating, please wait ..."""


_FiatWithdrawalInfo = """Please provide us with your card information
to do withdrawals (min amount is 100 CNY):"""

_SProc = "Please wait for your order to be processed ..."
_SProcClosed = "Trade closed."
