import pprint
import menu
import wallet.messaging as wm
import market.messaging as mm
import wallet.storage as w_db
from telegram import InlineKeyboardMarkup
import telegram
from hashlib import sha256
import messages as mes
import wallet.config as cfg
import web3 as web3
from wallet.withdrawal import WithdrawalManager
import logging
from decimal import Decimal
import gateways.alipay.connector as alipay
# from io import BytesIO
from content.langs import Message
import wallet.rates as rate

WALLET_JOB_DICT = {}
RUN_INTERVAL_WALLET = 10


logging.basicConfig(
                    format=mes.LogFormat,
                    level=logging.INFO)

logger = logging.getLogger(__name__)


def multi_lang(func):

    def _lookup_loc(kwargs):

        assert 'chat_data' in kwargs, "No chat_data key for multi_lang"
        return len([k for k in kwargs['chat_data'].keys() if k == 'loc']) == 1

    def _wrap(*args, **kwargs):
        print(args)
        print(kwargs)
        if _lookup_loc(kwargs):
            return func(*args, **kwargs)
        else:
            upd_args = [a for a in args if isinstance(a,
                        telegram.update.Update)]
            assert len(upd_args) == 1, 'Must have telegram.Update arg'
            uid = upd_args[0].effective_user.id
            kwargs['chat_data']['loc'] = Message(uid)
        return func(*args, **kwargs)
    return _wrap


def get_lang(chat_data, uid):

    if chat_data is None or 'loc' not in chat_data.keys():
        chat_data['loc'] = Message(uid)
    return chat_data['loc']


@multi_lang
def handle_assertions(e, bot, update, chat_data=None):

    logger.exception(e)

    mes = get_lang(chat_data, update.effective_user.id)

    reply_markup = InlineKeyboardMarkup([menu.get_main_menu(mes)])
    bot.send_message(chat_id=update.effective_user.id,
                     text="Failed to process: {0}, please try again."
                     .format(e),
                     parse_mode='HTML', reply_markup=reply_markup)


def handle_empty_chat_data(bot, update):

    bot.send_message(chat_id=update.effective_user.id,
                     text="Can't process multiple button click.",
                     parse_mode='HTML')


def _decode_base58(bc, length):
    digits58 = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'
    n = 0
    for char in bc:
        n = n * 58 + digits58.index(char)
    return n.to_bytes(length, 'big')


def _is_btc_addr(addr):

    try:
        bcbytes = _decode_base58(addr, 25)
        return bcbytes[-4:] == sha256(sha256(bcbytes[:-4])
                                      .digest()).digest()[:4]
    except Exception:
        return False


def _is_eth_eth(addr):

    return web3.Web3.isAddress(addr)


def _validate_address(addr):

    if (_is_btc_addr(addr)):
        return cfg.BC_COIN
    if (_is_eth_eth(addr)):
        return cfg.ETH_COIN
    return None


def callback_wallet_change(bot, job):

    pprint.pprint("Sending notifications to %r" % job.context)
    uid = job.context
    nfs = w_db.get_opnotifications(str(uid))

    pprint.pprint(nfs)
    for n in nfs:
        o = w_db.get_op_by_id(n.OpId)
        loc = Message(uid)
        txt = loc.get_notification(o, uid)
        bot.send_message(chat_id=uid, text=txt, parse_mode='HTML')
        w_db.remove_opnotification(n.Id)


def _stw(cmd, menu_item):
    return cmd.startswith('{0}_'.format(menu_item))


@multi_lang
def proc_back_root(cmd, bot, update, args, job_queue, chat_data):
    reply_markup = InlineKeyboardMarkup([menu.get_main_menu(chat_data['loc'])])
    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc'].get_main(),
                     parse_mode='HTML', reply_markup=reply_markup)


def proc_loc(cmd, bot, update, args, job_queue, chat_data=None):
    chat_data['loc'] = Message(update.effective_user.id, cmd.split('_')[1])
    proc_welcome(bot, update, args, job_queue, chat_data=chat_data)


def proc_starts_with_cmd(cmd, bot, update, args, job_queue, chat_data):

    cmd_sw_mapping = {
        menu.command["loc"]["root"]: proc_loc,
        menu.command["wallet"]["topup"]["root"]: proc_coin_topup,
        menu.command["wallet"]["withdraw"]["root"]: proc_coin_withdraw,
        menu.command["back"]["root"]: proc_back_root,
        menu.command["buy"]["root"]: proc_coin_buy,
        menu.command["sell"]["root"]: proc_coin_sell,
        menu.command["second"]["root"]: proc_second_coin,
        menu.command["confirm"]["trade"]: proc_trade,
        menu.command["confirm"]["withdraw"]: proc_withdrawal,
        menu.command["cancel"]["trade"]: cancel_trade,
        menu.command["cancel"]["withdraw"]: cancel_withdrawal,
        menu.command["market"]["root"]: proc_market_pair
    }

    cmd_key = [key for key in cmd_sw_mapping.keys()
               if cmd.startswith(key)]

    print("Cmd key: %s" % cmd_key)

    assert len(cmd_key) == 1, "Command %s is incorrect" % cmd

    proc_func = cmd_sw_mapping[cmd_key[0]]

    assert proc_func is not None, "Invalid function mapping structure"

    proc_func(cmd, bot, update, args, job_queue, chat_data=chat_data)


def proc_menu_tap(cmd, bot, update, job_queue, chat_data):

    args = []

    command_mapping = {
        menu.command["wallet"]["root"]: proc_wallet,
        menu.command["wallet"]["topup"]["root"]: proc_topup,
        menu.command["wallet"]["ops"]["root"]: proc_ops,
        menu.command["wallet"]["withdraw"]["root"]: proc_withdraw,
        menu.command["buy"]["root"]: proc_buy,
        menu.command["sell"]["root"]: proc_sell,
        menu.command["market"]["root"]: proc_market,
    }

    proc_func = command_mapping.get(cmd, None)

    print(proc_func)

    try:
        if (proc_func):
            proc_func(bot, update, args, job_queue, chat_data=chat_data)
        else:
            proc_starts_with_cmd(cmd, bot, update, args, job_queue, chat_data)
    except AssertionError as e:
        handle_assertions(e, bot, update, chat_data=chat_data)


def _parse_wallet_params(args):

    cmd = "wallet"
    if (len(args) > 0):
        cmd = args[0]

    return {"command": cmd}


@multi_lang
def proc_welcome(bot, update, args, job_queue, chat_data=None):
    reply_markup = InlineKeyboardMarkup([menu.get_main_menu(chat_data['loc'])])
    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc'].get_welcome(),
                     parse_mode='HTML', reply_markup=reply_markup)


@multi_lang
def proc_ops(bot, update, args, job_queue, chat_data):
    reply_markup = InlineKeyboardMarkup(
            [menu.get_wallet_menu(chat_data['loc'])])
    wp = wm.Process(bot, update.effective_user.id, args, reply_markup,
                    chat_data['loc'])
    wp.show_ops()


@multi_lang
def proc_wallet(bot, update, args, job_queue, chat_data):

    params = _parse_wallet_params(args)

    reply_markup = InlineKeyboardMarkup(
        [menu.get_wallet_menu(chat_data['loc'])])

    wp = wm.Process(bot, update.effective_user.id, args, reply_markup,
                    chat_data['loc'])

    if (params["command"].upper() == "WALLET"):
        wp.add_wallet()

    if (params["command"].upper() == "OPS"):
        wp.show_ops()

    if update.effective_user.id not in WALLET_JOB_DICT:
        job = job_queue.run_repeating(callback_wallet_change,
                                      interval=RUN_INTERVAL_WALLET,
                                      first=0,
                                      context=str(update.effective_user.id))
        WALLET_JOB_DICT[update.effective_user.id] = job


@multi_lang
def proc_withdraw(bot, update, args, job_queue, chat_data):

    reply_markup = InlineKeyboardMarkup([menu.get_coins_menu(
        menu.command["wallet"]["withdraw"]["root"], chat_data['loc'])])

    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc'].get_deposit_currency(),
                     parse_mode='HTML', reply_markup=reply_markup)


@multi_lang
def proc_topup(bot, update, args, job_queue, chat_data):

    reply_markup = InlineKeyboardMarkup([menu.get_coins_menu(
        menu.command["wallet"]["topup"]["root"], chat_data['loc'])])

    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc'].get_deposit_currency(),
                     parse_mode='HTML', reply_markup=reply_markup)


@multi_lang
def proc_buy(bot, update, args, job_queue, chat_data):

    reply_markup = InlineKeyboardMarkup([menu.get_coins_menu(
        menu.command["buy"]["root"], chat_data['loc'])])

    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc'].get_buy_coin(),
                     parse_mode='HTML', reply_markup=reply_markup)


@multi_lang
def proc_sell(bot, update, args, job_queue, chat_data):

    reply_markup = InlineKeyboardMarkup([menu.get_coins_menu(
        menu.command["sell"]["root"], chat_data['loc'])])

    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc'].get_sell_coin(),
                     parse_mode='HTML', reply_markup=reply_markup)


@multi_lang
def proc_fiat_coin_topup(coin, bot, update, args, job_queue, chat_data):
    cmd = chat_data["command"]
    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc']
                     .get_topup_fiat_amount(cmd.split('_')[1]),
                     parse_mode='HTML')


@multi_lang
def proc_crypto_coin_topup(coin, bot, update, args, job_queue, chat_data):

    reply_markup = InlineKeyboardMarkup(
        [menu.get_wallet_menu(chat_data['loc'])])

    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc'].get_wallet_address(coin),
                     parse_mode='HTML')

    w = w_db.get_wallet_by_uid(str(update.effective_user.id), coin.lower())

    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc'].get_wallet_address_info(w.Address),
                     parse_mode='HTML', reply_markup=reply_markup)


def proc_coin_topup(cmd, bot, update, args, job_queue, chat_data):

    cmd_parts = cmd.split('_')
    chat_data["command"] = cmd
    coin = cmd_parts[len(cmd_parts)-1]

    if (coin in cfg.FIAT_COINS):
        proc_fiat_coin_topup(coin, bot, update, args, job_queue,
                             chat_data=chat_data)
    else:
        proc_crypto_coin_topup(coin, bot, update, args, job_queue,
                               chat_data=chat_data)


@multi_lang
def proc_coin_withdraw(cmd, bot, update, args, job_queue, chat_data):
    chat_data["command"] = cmd
    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc']
                     .get_withdraw_coin_amount(cmd.split('_')[1]),
                     parse_mode='HTML')


@multi_lang
def proc_coin_buy(cmd, bot, update, args, job_queue, chat_data):
    chat_data["command"] = cmd
    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc']
                     .get_buy_coin_amount(cmd.split('_')[1]),
                     parse_mode='HTML')


@multi_lang
def proc_coin_sell(cmd, bot, update, args, job_queue, chat_data):
    chat_data["command"] = cmd
    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc']
                     .get_sell_coin_amount(cmd.split('_')[1]),
                     parse_mode='HTML')


@multi_lang
def proc_float_input_trade(bot, update, chat_data):

    chat_data["command"] += '_' + update.message.text + '_'
    reply_markup = InlineKeyboardMarkup([menu.get_coins_menu(
            menu.command["second"]["root"], chat_data['loc'])])

    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc']
                     .get_second_coin(chat_data["command"]),
                     parse_mode='HTML',
                     reply_markup=reply_markup)


def proc_float_input_withdraw(bot, update, chat_data):

    chat_data["command"] += '_' + update.message.text + '_'
    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc']
                     .get_withdraw_address(chat_data["command"]),
                     parse_mode='HTML')


@multi_lang
def proc_address_input_withdraw(bot, update, chat_data):

    cmd_parts = chat_data["command"].split('_')
    addr_coin = _validate_address(update.message.text)

    if (addr_coin == cmd_parts[1] or
       (cmd_parts[1] is not cfg.BC_COIN and addr_coin == cfg.ETH_COIN)):

        chat_data["command"] += update.message.text
        parts = chat_data["command"].split('_')
        reply_markup = InlineKeyboardMarkup(
                        [menu.get_confirmation_menu(parts[0],
                                                    chat_data['loc'])])

        bot.send_message(chat_id=update.effective_user.id,
                         text=chat_data['loc'].get_confirmation_withdraw(
                            Decimal(parts[2]),
                            parts[1],
                            parts[3]),
                         parse_mode='HTML',
                         reply_markup=reply_markup)
    else:
        bot.send_message(chat_id=update.effective_user.id,
                         text="Address is invalid. Please enter a valid one.",
                         parse_mode='HTML')


@multi_lang
def proc_float_fiat_topup(bot, update, chat_data):

    bot.send_message(update.effective_user.id,
                     text=chat_data['loc'].get_alipay_progress())

    chat_data["command"] += '_' + update.message.text
    parts = chat_data["command"].split('_')

    print(parts)

    fiat_top_up_mappings = {
        'cny': _top_up_cny,
        'ngn': _top_up_ngn
    }

    topup_func = fiat_top_up_mappings.get(parts[1], None)

    assert topup_func is not None, "TopUp function is not defined for %s" % parts[1]

    topup_func(parts[2], bot, update, chat_data=chat_data)


@multi_lang
def proc_float_fiat_withdraw(bot, update, chat_data):

    cmd = chat_data['command']

    cmd += '_' + update.message.text

    parts = cmd.split('_')

    try:

        al = alipay.AliPayConnector(str(update.effective_user.id), parts[1],
                                    logger=logger)

        reply_markup = InlineKeyboardMarkup(
            [menu.get_fiat_withdrawal(str(update.effective_user.id),
                                      al.get_payout_req(parts[2]),
                                      chat_data['loc'])])

        mes = chat_data['loc'].get_withdrawal_fiat_info()

        bot.send_message(chat_id=update.effective_user.id,
                         text=mes, parse_mode='HTML',
                         reply_markup=reply_markup)

    except AssertionError as e:
        handle_assertions(e, bot, update, chat_data=chat_data)


def proc_float_input(bot, update, chat_data):

    cmd = None

    if ("command" in chat_data):
        cmd = chat_data["command"]

    assert cmd is not None, "Anonymous float input, no cmd provided."

    cmd_parts = cmd.split('_')

    print(cmd_parts)

    if (cmd.startswith(menu.command["buy"]["root"])
       or cmd.startswith(menu.command["sell"]["root"])):
            proc_float_input_trade(bot, update, chat_data=chat_data)
    elif (cmd.startswith(menu.command["wallet"]["withdraw"]["root"])):
        if (cmd_parts[len(cmd_parts)-1] in cfg.FIAT_COINS):
            proc_float_fiat_withdraw(bot, update, chat_data=chat_data)
        else:
            proc_float_input_withdraw(bot, update, chat_data)
    elif (cmd.startswith(menu.command["wallet"]["topup"]["root"])
            and cmd_parts[len(cmd_parts)-1] in cfg.FIAT_COINS):
                proc_float_fiat_topup(bot, update, chat_data=chat_data)


@multi_lang
def proc_second_coin(cmd, bot, update, args, job_queue, chat_data):

    second_coin = cmd.split('_')[1].lower()
    cmd_parts = chat_data["command"].split('_')
    buy_sell = cmd_parts[0].lower()
    first_coin = cmd_parts[1].lower()
    amount = float(cmd_parts[2].lower())
    r = rate.get_rate('_'.join([first_coin, second_coin]))
    total = round(float(amount*r), 8)

    reply_markup = InlineKeyboardMarkup([menu.get_confirmation_menu('trade',
                                         chat_data['loc'])])

    w_second = w_db.get_wallet_by_uid(str(update.effective_user.id),
                                      second_coin.lower())

    w_first = w_db.get_wallet_by_uid(str(update.effective_user.id),
                                     first_coin.lower())

    chat_data["command"] += second_coin + '_'
    chat_data["command"] += str(r)

    mes = ''
    if (buy_sell == menu.command["buy"]["root"]):
        mes = chat_data['loc'].get_confirmation_buy_trade(
           amount, first_coin, r, second_coin, total, buy_sell,
           w_second.Balance)
    elif (buy_sell == menu.command["sell"]["root"]):
        mes = chat_data['loc'].get_confirmation_sell_trade(
            amount, first_coin, r, second_coin, total, buy_sell,
            w_first.Balance)

    bot.send_message(chat_id=update.effective_user.id,
                     text=mes, parse_mode='HTML',
                     reply_markup=reply_markup)


# sell[buy] 0.15 ETH rate 10.5 BTC
def _parse_exchange_params(args):

    assert len(args) == 5, "Arguments are not consistent, can't parse."
    assert args[2].upper() == "RATE", "Rate param is not set."

    pair = "{0}_{1}".format(args[1].upper(), args[4].upper())
    return {"volume": args[0], "symbol": pair, "price": args[3]}


@multi_lang
def proc_withdrawal(cmd, bot, update, args, job_queue, chat_data):

    if ("command" not in chat_data):
        handle_empty_chat_data(bot, update)
        return

    parts = chat_data["command"].split('_')
    del chat_data["command"]
    w_mng = WithdrawalManager(parts[1], str(update.effective_user.id))
    tx = None
    try:
        op = w_mng.request_withdraw(Decimal(parts[2]), parts[3])
        tx = w_mng.confirm_withdraw(op, Decimal(parts[2]))
    except AssertionError as e:
        handle_assertions(e, bot, update, chat_data=chat_data)

    if (tx):
        reply_markup = InlineKeyboardMarkup(
            [menu.get_main_menu(chat_data['loc'])])
        bot.send_message(chat_id=update.effective_user.id,
                         text=chat_data['loc']
                         .get_confirm_withdrawal(tx['tx'], parts[2],
                                                 parts[1], parts[3]),
                         parse_mode='HTML', reply_markup=reply_markup)


def proc_exchange(bot, update, args, job_queue, type, chat_data):

    params = _parse_exchange_params(args)
    reply_markup = InlineKeyboardMarkup([menu.get_main_menu(chat_data['loc'])])
    ep = wm.Process(bot, update.effective_user.id, args, reply_markup,
                    chat_data['loc'])
    ep.exchange(params["symbol"],
                float(params["volume"]),
                float(params["price"]),
                int(type))


def proc_trade(cmd, bot, update, args, job_queue, chat_data):

    if ("command" not in chat_data):
        handle_empty_chat_data(bot, update)
        return

    cmd_parts = chat_data["command"].split('_')

    del chat_data["command"]

    buy_sell = cmd_parts[0]

    type = -1

    if buy_sell == menu.command["buy"]["root"]:
        type = 1
    elif buy_sell == menu.command["sell"]["root"]:
        type = 0

    print(cmd_parts)
    # 0 vol
    args.append(float(cmd_parts[2].lower()))
    # 1 first_coin
    args.append(cmd_parts[1])
    # 2 rate
    args.append("rate")
    # 3 price
    args.append(float(cmd_parts[4]))
    # 4 second_coin
    args.append(cmd_parts[3])

    proc_exchange(bot, update, args, job_queue, type, chat_data=chat_data)


@multi_lang
def cancel_trade(cmd, bot, update, args, job_queue, chat_data):

    del chat_data["command"]

    reply_markup = InlineKeyboardMarkup([menu.get_main_menu(chat_data['loc'])])

    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc'].get_cancel_trade(),
                     parse_mode='HTML',
                     reply_markup=reply_markup)


@multi_lang
def cancel_withdrawal(cmd, bot, update, args, job_queue, chat_data):

    del chat_data["command"]

    reply_markup = InlineKeyboardMarkup([menu.get_main_menu(chat_data['loc'])])

    bot.send_message(chat_id=update.effective_user.id,
                     text=chat_data['loc'].get_cancel_trade(),
                     parse_mode='HTML',
                     reply_markup=reply_markup)


@multi_lang
def _top_up_cny(amount, bot, update, chat_data):
    # al = alipay.AliPayConnector(str(update.effective_user.id), parts[1],
    #                            logger=logger)
    # img_r = al.get_payment_qr(Decimal(parts[2]))
    # img = BytesIO(img_r)

    mes = chat_data['loc'].get_alipay_topup()

    reply_markup = InlineKeyboardMarkup(
        [menu.get_fiat_alipay_topup(str(update.effective_user.id),
                                    amount,
                                    chat_data['loc'])])

    bot.send_message(chat_id=update.effective_user.id,
                     text=mes, parse_mode='HTML',
                     reply_markup=reply_markup)

    # bot.send_photo(chat_id=update.effective_user.id, photo=img)


@multi_lang
def _top_up_ngn(amount, bot, update, chat_data):
    return None


@multi_lang
def proc_market(bot, update, args, job_queue, chat_data):

    menu_items = menu.get_market_menu('market')

    menu_items_1 = menu_items[:len(menu_items)//2]
    menu_items_2 = menu_items[len(menu_items)//2:]

    reply_markup = InlineKeyboardMarkup([menu_items_1, menu_items_2])

    wp = mm.Process(bot, update.effective_user.id, args, reply_markup,
                    chat_data['loc'])

    wp.show()


@multi_lang
def proc_market_pair(cmd, bot, update, args, job_queue, chat_data):

    print(cmd)
    reply_markup = InlineKeyboardMarkup(
        [menu.get_main_menu(chat_data['loc'])])

    wp = mm.Process(bot, update.effective_user.id, args, reply_markup,
                    chat_data['loc'])

    cmd_parts = cmd.split('_')

    pair = '%s_%s' % (cmd_parts[1], cmd_parts[2])
    print(pair)

    wp.get_market(pair)
