import requests
import wallet.config as cfg
import auth.token as token


def get_rate(lowed_case_pair):

    url = "%ssymbols" % cfg.EXCHANGE_PROVIDER

    headers = {
        'Authorization': "Bearer %s" % token.get_token(),
        'Cache-Control': "no-cache",
        'Accept': 'application/json',
    }

    response = requests.request("GET", url, headers=headers, verify=False)

    print(response.__dict__)

    arr = response.json()

    res = [a for a in arr if a['symbol'] == lowed_case_pair.upper()]

    return res[0]['price']
