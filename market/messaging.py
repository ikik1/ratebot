import messages as global_mes
import logging
import storage as db_ticks
from bot import send_top_5_tokens
import auth.token as auth
import wallet.config as cfg
import requests
import json

logging.basicConfig(
                    format=global_mes.LogFormat,
                    level=logging.INFO)

logger = logging.getLogger(__name__)


class Process:

    def __init__(self, bot, uid, args, reply_markup, loc):
        if bot is None:
            raise ValueError("Bot object is not defined")
        if not uid:
            raise ValueError("User identifier is not defined")
        self.uid = uid
        self.bot = bot
        self.args = args
        self.markup = reply_markup
        self.loc = loc

    def _show_current_btc_price(self):

        ticks = db_ticks.get_ticks()
        chat = self.bot.getChat(self.uid)
        send_top_5_tokens(self.bot, chat.id, ticks)

    def show(self):

        # showing current bitcoin price
        self._show_current_btc_price()

        # choosing market pair
        txt = self.loc.get_choose_market_text()
        chat = self.bot.getChat(self.uid)

        self.bot.send_message(chat_id=chat.id, text=txt, parse_mode='HTML',
                              reply_markup=self.markup)

    def get_market(self, pair):

        headers = {'Authorization': "Bearer %s" % auth.get_token()}
        print(headers)

        url = "{0}{1}/orders?fromDate=0".format(cfg.EXCHANGE_PROVIDER, pair)
        r = requests.get(url, headers=headers)

        jr = json.loads(r.text)

        # choosing market pair
        txt = self.loc.get_orders_list_text(jr, pair)
        chat = self.bot.getChat(self.uid)

        self.bot.send_message(chat_id=chat.id, text=txt, parse_mode='HTML',
                              reply_markup=self.markup)

        # https://obapi.stg.x-2.io/BTC_ETH/orders?fromDate=0
