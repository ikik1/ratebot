import unittest
import requests
import json

USER_ID = '140506929'
USER_ID_EX = '510870150'
API_ENDPOINT = 'http://127.0.0.1:7778/api/'
CUR_BTC = 'btc'
CUR_ETH = 'eth'
AMOUNT = 0.017
AMOUNT_EX = 0.1
TOPUP_AMOUNT = 100000000
BTC_ADDRESS = 'CA76tRxTXHTBpQ5fxm24y8oosaABCAd4nP'

BTC_TOP_UP_LOAD = '''{
    "outputs": [
                {
                 "value":"100000000",
                 "addresses":["CA76tRxTXHTBpQ5fxm24y8oosaABCAd4nP"]
                }
               ],
    "inputs": [{"addresses":[]}],
    "hash": "test-topup-hash"
}'''


class ServerBTCTopUpTest(unittest.TestCase):

    def test_topup(self):
        url = '{0}topup'.format(API_ENDPOINT)
        data = json.loads(BTC_TOP_UP_LOAD)
        r = requests.post(url, json=data, verify=False)

        self.assertEqual(r.status_code, 200)


class ServerBTCETHExchange(unittest.TestCase):

    def test_normal(self):

        url = '{0}hold'.format(API_ENDPOINT)
        r = requests.post(url, json={
                  "request-service": "order-book",
                  "operation-id": "test-op",
                  "user-id": USER_ID,
                  "amount": AMOUNT,
                  "currency": CUR_BTC
        }, verify=False)

        hold_result = r.json()
        print(hold_result["message"])
        self.assertEqual(r.status_code, 200)
        self.assertEqual(hold_result["result"], "true")
        self.assertTrue("hold-id" in hold_result)

        hold_id1 = hold_result["hold-id"]

        r = requests.post(url, json={
                "request-service": "order-book",
                "operation-id": "test-op",
                "user-id": USER_ID_EX,
                "amount": AMOUNT_EX,
                "currency": CUR_ETH
        }, verify=False)

        hold_result = r.json()
        print(hold_result["message"])
        self.assertEqual(r.status_code, 200)
        self.assertEqual(hold_result["result"], "true")
        self.assertTrue("hold-id" in hold_result)

        hold_id2 = hold_result["hold-id"]

        url = '{0}exchange'.format(API_ENDPOINT)
        r = requests.post(url, json={
                "request-service": "order-book",
                "operation-id": "test-op",
                "hold-id-from": hold_id1,
                "amount-from": AMOUNT,
                "hold-id-to": hold_id2,
                "amount-to": AMOUNT_EX,
        }, verify=False)

        hold_result = r.json()
        print(hold_result["message"])
        self.assertEqual(r.status_code, 200)
        self.assertEqual(hold_result["result"], "true")


class ServerBTCHoldReleaseTest(unittest.TestCase):

    def test_normal(self):

        url = '{0}hold'.format(API_ENDPOINT)
        r = requests.post(url, json={
                  "request-service": "order-book",
                  "operation-id": "test-op",
                  "user-id": USER_ID,
                  "amount": AMOUNT,
                  "currency": CUR_BTC
        }, verify=False)

        hold_result = r.json()
        print(hold_result["message"])

        self.assertEqual(r.status_code, 200)
        self.assertEqual(hold_result["result"], "true")
        self.assertTrue("hold-id" in hold_result)
        hold_id = hold_result["hold-id"]

        url = '{0}release'.format(API_ENDPOINT)
        r = requests.post(url, json={
                "request-service": "order-book",
                "operation-id": "test-op",
                "hold-id": hold_id,
                "amount": AMOUNT
        }, verify=False)

        release_result = r.json()

        print(hold_result["message"])

        self.assertEqual(r.status_code, 200)
        self.assertEqual(release_result["result"], "true")
        self.assertTrue("op-id" in release_result)


if __name__ == '__main__':
    unittest.main()
