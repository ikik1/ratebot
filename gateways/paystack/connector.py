import wallet.config as cfg
import wallet.storage as db
from wallet.operations import WalletOps
from decimal import Decimal
import datetime
from dotenv import load_dotenv
import os
import hashlib
import requests
from bs4 import BeautifulSoup
import flask
from sparkpost import SparkPost


load_dotenv()


class NgnConnector():

    def __init__(self, uid, coin, logger=None):

        assert uid is not None, "UserId can't be None"
        assert coin is not None, "Coin can't be None"

        wallet = db.get_wallet_by_uid(uid, coin)

        logger.info("Wallet: %s (%s:%s)" % (wallet, uid, coin))

        if (wallet is None):
            wallet = db.new_wallet(uid, coin, type(self).__name__,
                                   type(self).__name__, '')
        self.wallet = wallet
        self.logger = logger

    def _get_commission_value(self, amount):

        return amount*(Decimal(cfg.WITHDRAWAL_CP[self.coin]/100)) \
               + Decimal(cfg.WITHDRAWAL_CF[self.coin])

    def get_payout_req(self, amount):

        amount = Decimal(amount)
        ca = self._get_commission_value(amount)
        assert self.wallet.Balance >= ca+amount, \
            "Balance is not enough (commission is: %s %s)" % (str(ca),
                                                              self.wallet.Coin)

        return str(db.add_FiatPaymentRequest(
            self.wallet.UserId,
            Decimal(amount),
            self.wallet.Coin
        ).Id)

    def do_payout_request(self, amount, accname, cardnumber, bvn,
                          address, email, orderid):

        sp = SparkPost(os.getenv("SPARKPOST_EMAIL_KEY"))

        html = flask.render_template('paystack/email.html',
                                     amount=amount,
                                     accountname=accname,
                                     cardnumber=cardnumber,
                                     bvn=bvn,
                                     address=address)

        sp.transmissions.send(
            recipients=[os.getenv('EMAIL_FIN_NOTIFICATIONS'), email],
            html=html,
            from_email='notifier.xs@xsquared.app',
            subject='[XSquared] Withdrawal request (%s)' % orderid
        )

        return {'status': 'Success', 'msg': 'Withdrawal is being processed',
                'transaction_id': '%s: %s' % (email, orderid)}

    def get_payload(self, amount):

        wo = WalletOps(coin=self.wallet.Coin, uid=self.wallet.UserId)
        amount = wo.process_amount_from_normal_dec(Decimal(amount))

        payload = {
            'pay_amount': str(amount),
            'pay_applydate': str(datetime.datetime.utcnow()),
            'pay_bankcode': '903',
            'pay_callbackurl': '',
            'pay_memberid': os.getenv("ALIPAY_MEMID"),
            'pay_notifyurl': cfg.ALIPAY_NOTIFICATION_URL,
            'pay_orderid': str(db.add_FiatPaymentRequest(
                self.wallet.UserId,
                Decimal(amount),
                self.wallet.Coin
            ).Id),
            'pay_productname': 'XSQUARED PURCHASE'
        }

        s_sign = "pay_amount=" + payload['pay_amount'] + "&pay_applydate=" \
                               + payload['pay_applydate'] + "&pay_bankcode=" \
                               + payload['pay_bankcode'] + "&pay_memberid=" \
                               + payload['pay_memberid'] + "&pay_notifyurl=" \
                               + payload['pay_notifyurl'] + "&pay_orderid=" \
                               + payload['pay_orderid'] + "&key=" \
                               + os.getenv("ALIPAY_KEY") + ""

        m = hashlib.md5()
        m.update(s_sign.encode('utf-8'))
        pay_md5sign = m.hexdigest().upper()
        payload.update({'pay_md5sign': pay_md5sign})

        return payload

    def get_payment_qr(self, amount):

        assert amount > 0, "Amound can't be zero"

        payload = self.get_payload(amount)
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}

        req = requests.Request('POST', 'https://www.kkhpay.com/Pay_Index.html',
                               headers=headers, data=payload)
        pr = req.prepare()

        self.logger.info("Request headers: %s" % pr.headers)
        self.logger.info("Request body: %s" % pr.body)

        s = requests.Session()
        r = s.send(pr)

        self.logger.info("Response content: %s" % r.text)

        soup = BeautifulSoup(r.text, 'html.parser')

        qr_div = soup.find(id="qrcode")

        self.logger.info("QR div: %s" % qr_div)

        if (qr_div is None):
            return flask.Response("Fail")

        qr_url = None

        imgs = qr_div.findChildren("img")

        self.logger.info("Child: %s" % imgs)

        l_img = sum(1 for _ in imgs)

        self.logger.info("Len imgs: %s" % l_img)

        if (l_img == 2):
            if (cfg.ALIPAY_QR_URL != ''):
                qr_url = cfg.ALIPAY_QR_URL % imgs[1]['src']
            else:
                qr_url = imgs[1]['src']

        if (qr_url is None):
            return flask.Response("Fail")

        img_r = requests.get(qr_url)
        self.logger.info("Response IMG content: %s" % img_r.content)

        return img_r.content

    def __notify(self, op, target_wallet=None):
        db.add_opnotification(op.UserId, op.Id)

    def confirm(self, fiat_rq, ext_ref):

        assert fiat_rq is not None, "Ref can't be None"
        assert ext_ref is not None, "External Ref can't be None"

        assert fiat_rq.Cfms == 0, "Payment request has already been confirmed"

        wo = WalletOps(coin=fiat_rq.Coin, uid=fiat_rq.UserId)
        op = wo.roll(db.OpTypes.TopUp, Decimal(str(fiat_rq.Amount)),
                     reference=ext_ref,
                     callback=self.__notify, to_norm_dec=True)

        db.confirm_fiatpaymentrequest(fiat_rq.Id)

        return op
