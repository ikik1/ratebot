_Welcome = """
XSquared(X2)交易宝宝是接驳了X2全流量交易技术的平台，能为您提供以下服务:

1) 数字资产交易。
2) 实时显示数字资产市场的数据及信息。
3) 全功能钱包, 可自由存入及提取数字资产。
4) 及更多。

你所有的在账户上的资产将会保存在，多签的冷钱包上，确保安全，并且可以在公共区块链搜寻器上查看得到。

请在电报(Telegram)的交易宝宝的页面输入/start，便能看得到所有的指令选项。

多谢你成为X2大家庭的一分子, 我们希望你能享受这简单又方便的交易方式!

X2团队献上
"""

_Menu = {"wallet": "钱包", "buy": "买", "sell": "卖",
         "top-up": "充值",
         "withdraw": "提现", "history": "交易记录列表", "options": "返回",
         "cny": "CNY", "ngn": "NGN", "confirm": "确认", "cancel": "取消", "market": "市场"}


_Main = "请选择选项"

_Market = "Choose a market pair:"

_Market_Orders = """
--== <b>{0}</b> open orders ==--
{1}
"""

_Market_Order = """
Type: {0}
Volume: {1}
Price: {2}
"""

_Order_Types = {0: "卖", 1: "买"}
