import requests

API = "https://api.coinmarketcap.com/v1/"

def get_ticks():
    r = requests.get(API + "ticker/")
    return r.json()
