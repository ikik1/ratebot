import wallet.operations as op
import messages as global_mes
import logging
from gateways.alipay.connector import AliPayConnector
from gateways.paystack.connector import NgnConnector


logging.basicConfig(
                    format=global_mes.LogFormat,
                    level=logging.INFO)

logger = logging.getLogger(__name__)


class Process:

    def __init__(self, bot, uid, args, reply_markup, loc):
        if bot is None:
            raise ValueError("Bot object is not defined")
        if not uid:
            raise ValueError("User identifier is not defined")
        self.uid = uid
        self.bot = bot
        self.args = args
        self.markup = reply_markup
        self.loc = loc

    def show_ops(self):

        w_ops = op.OpOps(str(self.uid))
        ops = w_ops.get_ops()

        txt = self.loc.get_ops_summary(ops, str(self.uid))
        chat = self.bot.getChat(self.uid)

        self.bot.send_message(chat_id=chat.id, text=txt, parse_mode='HTML')

    def exchange(self, symbol, volume, price, type):

        eops = op.ExchangeOps(symbol, volume, price, type, str(self.uid))
        res = eops.post_trade()

        txt = self.loc.get_trade_summary(res, symbol)
        chat = self.bot.getChat(self.uid)

        self.bot.send_message(chat_id=chat.id, text=txt, parse_mode='HTML',
                              reply_markup=self.markup)

    def add_wallet(self):

        wallets = op.WalletOps.create_wallets(str(self.uid))

        # temprorary solution
        cn = AliPayConnector(str(self.uid), 'cny', logger=logger)
        ngn = NgnConnector(str(self.uid), 'ngn', logger=logger)

        wallets += [cn.wallet, ngn.wallet]

        txt = self.loc.get_wallet_summary(wallets)
        chat = self.bot.getChat(self.uid)

        self.bot.send_message(chat_id=chat.id, text=txt, parse_mode='HTML',
                              reply_markup=self.markup)
