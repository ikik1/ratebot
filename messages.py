LogFormat = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"

_CoinShortInfo = "<b>{0}:</b>\nUSD rate: {1}\nChange rate: {2}%\n"

_PriceChangeInfo = """
<b>{0}:</b>
Current rate: {2} USD
Previous rate: {1} USD
Difference : {3} USD
Changed: <b>{4}%</b>
"""

_PriceStopLossInfo = """
<b>{0} check price:</b>
Current rate: {2} USD
Previous rate: {1} USD
Check price: {3} USD

"""

_Welcome = """
Hello I am CryptoAlerts Bot!
\nI am here to help you track the latest changes in crypto/token rates
as well as notify you about your protfolio latest changes.
\nYou can access coin rates or subscribe to the notifications for a rate change by using:
\n- /get [coin short name (optional)]
(gets current coin rate and some stats)
\n- /notify [coin short name (optional)]
(sets notification when coin's rate changes over N%, ex. /notify btc 5)
\n- /remove [coin symbol (optional)]
(removes all notifications for the coin)
\n- /list
(gets all notifications)
\n- /check [coin symbol] [price in USD]
(notifies you everytime coin rate crosses a threshold of N USD, ex. /stop btc 10000)
\n- /start
(shows this message, use it for getting help about these commands)
\nYou can customize your notifications using extra params within /notify method.
\nFor example, if you want receive notifications only for BTC with 1% change rate, use the following params along with /notify command: /notify BTC 1
"""

NotificationAdded = "Done. (Use /list command to list your notifications)"
NotificationSetting = "Setting notifications ..."

_JobList = """
List of registered notifications:
\n{0}
\nTo remove subscription use /remove [token symbol]
"""
_JobListRemoval = """
The following notifications have been removed:
\n{0}
"""

_DetailedTokenInfo = """
<b>{:s} ({:s})</b>:
USD rate: {:.2f}
BTC rate: {:.2f}
7d change rate: {:.2f}%
24h change rate: {:.2f}%
1h change rate: {:.2f}%
USD cap: {:,.0f}
Total supply: {:,.0f}
"""

_NoJobs = "No subscriptions found, try create one by calling /notify"

_NotificationFriendlyName = "<b>{0}</b> with {1}% threshold"
_NotificationStopLossName = "<b>{0}</b> check price at {1} USD"

PortfolioUpdated = "Done. (Use /book list command to display your portfolio)"

_PortfolioTotalsList = """
Your portfolio:
{0}
<b>Totals</b>
Spent volume: <b>{1}</b> USD
Current vol.: <b>{2}</b> USD
Profit: <b>{3}</b> USD
Growth rate: <b>{4}</b>%
\nuse /book buy[sell] [amount] [token name] [price] [exchange token/currency]
to modify your portfolio.
"""

_PortfolioTotals = """
<b>{0}</b>
Token qnt.:<b>{1}</b>
Spent: <b>{2}</b> USD
Avrg. buy rate: <b>{3}</b> USD
Current vol.: <b>{4}</b> USD
Profit: <b>{5}</b> USD
Growth rate: <b>{6}</b>%
"""

_NoTotals = """
Your portfolio is empty.
Use /book buy[sell] [amount] [token name] [price] [exchange token/currency]
to modify your portfolio
"""

_Main = "Please choose an option:"

def get_coin_short_info(name, price_usd, change):
    return _CoinShortInfo.format(name, price_usd, change)


def get_welcome():
    return _Welcome


def get_notification_list_info(notifications):
    if len(notifications) == 0:
        return _NoJobs
    return _JobList.format('\n'.join(notifications))


def get_totals_list_info(totals, aggr):
    if len(totals) == 0:
        return _NoTotals
    return _PortfolioTotalsList.format('\n'.join(totals),
                                       bgnf(aggr["TotalVolume"]),
                                       bgnf(aggr["CurrentVolume"]),
                                       bgnf(aggr["Profit"]),
                                       "%.2f" % aggr["ChangeRate"])


def get_notification_list_removal(jobids):
    if len(jobids) == 0:
        return _NoJobs
    return _JobListRemoval.format('\n'.join(jobids))


def get_detailed_info(name, symb, rate_USD,
                      rate_BTC, ch7, ch24, ch1, capUSD, suppl):
    return _DetailedTokenInfo.format(name, symb, rate_USD,
                                     rate_BTC, ch7, ch24, ch1, capUSD,
                                     suppl)


def get_notification_friendly_name(notification):
    if (float(notification.ThreshHoldPrice) == 0):
        return _NotificationFriendlyName.format(
            notification.Token,
            notification.ThresholdPercent)
    if (float(notification.ThreshHoldPrice) > 0):
        return _NotificationStopLossName.format(
            notification.Token,
            notification.ThreshHoldPrice)


def get_price_change_info(name, before, now, difference, change):
    return _PriceChangeInfo.format(name, "%.2f" % before,
                                   "%.2f" % now, "%.2f" % difference,
                                   "%.2f" % change)


def get_stop_loss_change_info(name, before, now, stoploss_price):
    return _PriceStopLossInfo.format(name, "%.2f" % before,
                                     "%.2f" % now, "%.2f" % stoploss_price)


def bgnf(n):
    return "{:,.0f}".format(n)


def get_totals_friendly_name(portfolio_totals, aggr):
    return _PortfolioTotals.format(
        portfolio_totals.Token.upper(),
        "%.8f" % portfolio_totals.TotalAmount,
        bgnf(portfolio_totals.TotalVolume),
        "%.2f" % portfolio_totals.AvrgPrice,
        bgnf(aggr["CurrentVolume"]),
        bgnf(aggr["Profit"]),
        "%.2f" % aggr["ChangeRate"]
    )


def get_main():
    return _Main
