import wallet.config as cfg
import wallet.storage as db
from wallet.operations import WalletOps
from decimal import Decimal
from dotenv import load_dotenv
import os
import hashlib
import requests
from bs4 import BeautifulSoup
import flask
import base64

from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA
from Crypto.PublicKey import RSA


load_dotenv()


class OnePayConnector():

    def __init__(self, uid, coin, logger=None):

        assert uid is not None, "UserId can't be None"
        assert coin is not None, "Coin can't be None"

        wallet = db.get_wallet_by_uid(uid, coin)

        logger.info("Wallet: %s (%s:%s)" % (wallet, uid, coin))

        if (wallet is None):
            wallet = db.new_wallet(uid, coin, type(self).__name__,
                                   type(self).__name__, '')
        self.wallet = wallet
        self.logger = logger

    def _get_commission_value(self, amount):

        return amount*(Decimal(cfg.WITHDRAWAL_CP["cny"]/100)) \
               + Decimal(cfg.WITHDRAWAL_CF["cny"])

    def get_payout_req(self, amount):

        amount = Decimal(amount)
        ca = self._get_commission_value(amount)
        assert self.wallet.Balance >= ca+amount, \
            "Balance is not enough (commission is: %s %s)" % (str(ca),
                                                              self.wallet.Coin)

        return str(db.add_FiatPaymentRequest(
            self.wallet.UserId,
            Decimal(amount),
            self.wallet.Coin
        ).Id)

    def do_payout_request(self, amount, bname, branch, name, card, province,
                          city, ex, payment_id):

        s_ext = '{"lhh":"%d"}' % int(ex)

        self.logger.info(s_ext)
        self.logger.info(s_ext.encode('ascii'))
        self.logger.info(base64.b64encode(s_ext.encode('ascii')))

        payload = {
            'mchid': os.getenv("ALIPAY_MEMID"),
            'out_trade_no': payment_id,
            'money': '%.2f' % Decimal(amount),
            'bankname': bname,
            'subbranch': branch,
            'accountname': name,
            'cardnumber': card,
            'province': province,
            'city': city,
            'extends': base64.b64encode(s_ext.encode('ascii')).decode('ascii')
        }

        payload_list = [(k, payload[k]) for k in sorted(payload.keys())]

        payload_list.append(('key', os.getenv("ALIPAY_KEY")))

        s_sign = '&'.join('{0}={1}'.format(
            key, val) for (key, val) in payload_list)

        self.logger.info(s_sign)

        m = hashlib.md5()
        m.update(s_sign.encode('utf-8'))
        pay_md5sign = m.hexdigest().upper()

        payload.update({'pay_md5sign': pay_md5sign})

        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        req = requests.Request('POST',
                               'https://www.kkhpay.com/Payment_Dfpay_add.html',
                               headers=headers, data=payload)

        pr = req.prepare()

        self.logger.info("Payload: %s" % payload)
        self.logger.info("Request headers: %s" % pr.headers)
        self.logger.info("Request body: %s" % pr.body)

        s = requests.Session()
        r = s.send(pr)

        self.logger.info(r)
        self.logger.info("Response content: %s" % r.text)
        self.logger.info("Response content: %s" % r.__dict__)

        return r

    def _post_req(self, url, headers, payload):

        req = requests.Request('POST', url, headers=headers, data=payload)

        pr = req.prepare()

        self.logger.info("Payload: %s" % payload)
        self.logger.info("Request headers: %s" % pr.headers)
        self.logger.info("Request body: %s" % pr.body)

        s = requests.Session()
        r = s.send(pr)

        self.logger.info(r)
        self.logger.info("Response content: %s" % r.text)
        self.logger.info("Response content: %s" % r.__dict__)

        return r

    def _sign_payload(self, pl):

        sorted_q_str = '&'.join('%s=%s' % (key, pl[key]) for key in sorted(pl))
        self.logger.info("Sorted params: %s" % sorted_q_str)
        pk = os.getenv("ONEPAY_PK")
        key = RSA.importKey(base64.b64decode(pk))
        h = SHA.new(sorted_q_str.encode())
        signer = PKCS1_v1_5.new(key)
        signature = signer.sign(h)
        b64sign = base64.b64encode(signature).hex()
        self.logger.info("Signed params: %s" % b64sign)
        return b64sign

    def get_payload(self, amount):

        order_num = str(db.add_FiatPaymentRequest(
                            self.wallet.UserId,
                            Decimal(amount),
                            self.wallet.Coin
                    ).Id)

        payload = {
            'amountFee': str(Decimal(amount)),
            'deviceType': 'H5',
            'version': '1.0',
            'issuingBank': 'UNIONPAY',
            'payType': 'NC',
            'merchantId': os.getenv("ONEPAY_MID"),
            'notifyUrl': cfg.ONEPAY_NOTIFICATION_URL,
            'currency': 'CNY',
            'inputCharset': 'UTF-8',
            'goodsTitle': 'XSQUARED_PURCHASE',
            'merchantTradeId': order_num,
            'returnUrl': cfg.ONEPAY_NOTIFICATION_URL,
            'cardType': 'D'
        }

        payload.update({'sign': self._sign_payload(payload)})
        payload.update({'signType': 'RSA'})

        payload.update({'url': cfg.ONEPAY_URL})

        return payload

    def get_payment_qr(self, amount):

        assert amount > 0, "Amound can't be zero"

        payload = self.get_payload(amount)
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}

        req = requests.Request('POST', 'https://www.kkhpay.com/Pay_Index.html',
                               headers=headers, data=payload)
        pr = req.prepare()

        self.logger.info("Request headers: %s" % pr.headers)
        self.logger.info("Request body: %s" % pr.body)

        s = requests.Session()
        r = s.send(pr)

        self.logger.info("Response content: %s" % r.text)

        soup = BeautifulSoup(r.text, 'html.parser')

        qr_div = soup.find(id="qrcode")

        self.logger.info("QR div: %s" % qr_div)

        if (qr_div is None):
            return flask.Response("Fail")

        qr_url = None

        imgs = qr_div.findChildren("img")

        self.logger.info("Child: %s" % imgs)

        l_img = sum(1 for _ in imgs)

        self.logger.info("Len imgs: %s" % l_img)

        if (l_img == 2):
            if (cfg.ALIPAY_QR_URL != ''):
                qr_url = cfg.ALIPAY_QR_URL % imgs[1]['src']
            else:
                qr_url = imgs[1]['src']

        if (qr_url is None):
            return flask.Response("Fail")

        img_r = requests.get(qr_url)
        self.logger.info("Response IMG content: %s" % img_r.content)

        return img_r.content

    def __notify(self, op, target_wallet=None):
        db.add_opnotification(op.UserId, op.Id)

    def confirm(self, fiat_rq, ext_ref):

        assert fiat_rq is not None, "Ref can't be None"
        assert ext_ref is not None, "External Ref can't be None"

        assert fiat_rq.Cfms == 0, "Payment request has already been confirmed"

        wo = WalletOps(coin=fiat_rq.Coin, uid=fiat_rq.UserId)
        op = wo.roll(db.OpTypes.TopUp, Decimal(str(fiat_rq.Amount)),
                     reference=ext_ref,
                     callback=self.__notify, to_norm_dec=False)

        db.confirm_fiatpaymentrequest(fiat_rq.Id)

        return op
